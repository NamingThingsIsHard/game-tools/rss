#![allow(dead_code, unused_imports)]

extern crate tokio;

use crate::binary_seeker::BinarySeeker;
use crate::packet::Packet;
use crate::security::Security;
use std::io::ErrorKind;
use std::net::SocketAddr;
use tokio::io;
use tokio::io::Interest;
use tokio::net::{TcpListener, TcpSocket, TcpStream};

// const LOCALHOST: &str = "127.0.0.1";
// const DEFAULT_PORT: u64 = 15779;
const MAX_PACKET_SIZE: usize = 8192;

pub struct LocalHost {
	s: Option<TcpStream>,
	listen_s: Option<TcpListener>,
	pub security: Security,
	pub on_connect: Option<fn(&str, u16)>,
	pub on_disconnect: Option<fn()>,
	pub on_recv: Option<fn(packet: Packet)>,
}

impl Default for LocalHost {
	fn default() -> Self {
		Self::new()
	}
}

impl LocalHost {
	pub fn new() -> Self {
		Self {
			s: None,
			listen_s: None,
			security: Security::new(),
			on_connect: None,
			on_disconnect: None,
			on_recv: None,
		}
	}

	pub async fn ready_to_read(self) -> bool {
		if !self.s {
			false
		} else {
			self.s?.ready(Interest::READABLE)
		}
	}

	pub async fn ready_to_write(self) -> bool {
		if !self.s {
			false
		} else {
			self.s?.ready(Interest::WRITABLE)
		}
	}

	pub async fn connect(&mut self, host: &str, port: u16) {
		self.close();
		self.s = Some(
			TcpStream::connect((host, port))
				.await
				.expect("Couldn't connect to the server..."),
		);
		self.security = Security::new();
	}

	/// Listen for next connection
	/// Limited to 1 connection at a time.
	pub async fn listen(&mut self, host: &str, port: u16) -> io::Result<()> {
		self.close();
		if !self.listen_s.is_none() {
			self.listen_s = None;
		}

		let addr: SocketAddr = format!("{:?}:{:?}", host, port.to_string())
			.parse()
			.unwrap();
		let socket = TcpSocket::new_v4()?;
		socket.bind(addr)?;
		self.listen_s = Some(socket.listen(1)?);

		let (socket, _) = self.listen_s.as_ref().unwrap().accept().await?;
		self.accept(socket)?;
		Ok(())
		// tokio::spawn(async move {
		// 	self.recv().await;
		// });
	}

	pub fn accept(&mut self, socket: TcpStream) -> io::Result<()> {
		self.s = Some(socket);
		self.security = Security::new();
		self.security.generate_security(true, true, true);
		Ok(())
	}

	pub async fn recv(&mut self) {
		let mut data = [0u8; MAX_PACKET_SIZE];
		loop {
			self.s.as_ref().unwrap().readable().await.unwrap();
			match self.s.as_ref().unwrap().try_read(&mut data) {
				Ok(0) => break,
				Ok(_n) => {
					self.security.recv(&data);
				}
				Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
					continue;
				}
				Err(e) => {
					eprintln!("failed to read from client socket; err = {:?}", e);
					return; // Err(e.into());
				}
			}
		}
		// Ok(())
	}

	pub async fn write(&mut self) {
		loop {
			self.s.as_ref().unwrap().writable().await.unwrap();
			// See if there are packets to be sent
			let packets = self.security.get_packets_to_send();

			// Send each packet in the list
			for mut p in packets {
				println!("p->s:{:?}", p.get_bytes());
				match self.s.as_ref().unwrap().try_write(&p.get_bytes()) {
					Ok(_) => continue,
					Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
						continue;
					}
					Err(e) => {
						eprintln!("Failed to write to client socket; err = {:?}", e);
						return; // Err(e.into());
					}
				}
			}
		}
	}

	pub async fn send(&mut self, mut data: Vec<u8>) -> io::Result<()> {
		while !data.is_empty() {
			self.s.as_ref().unwrap().writable().await?;
			let sent = self.s.as_ref().unwrap().try_write(data.as_slice()).unwrap();
			data.drain(0..sent);
		}
		Ok(())
	}

	pub fn close(&mut self) {
		if !self.s.is_none() {
			self.s = None;
			self.security = Security::new();
		}
	}
}
