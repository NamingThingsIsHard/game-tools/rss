extern crate tokio;

use crate::binary_seeker::BinarySeeker;
use crate::security::Security;
use std::io::ErrorKind;
use tokio::io::Interest;
// use tokio::io;
use tokio::net::{TcpListener, TcpStream};

const LOCALHOST: &str = "127.0.0.1";
const DEFAULT_PORT: u64 = 15779;
const MAX_PACKET_SIZE: usize = 8192;

pub struct RemoteHost {
	s: Option<TcpStream>,
	listen_s: Option<TcpListener>,
	pub security: Security,
}

impl Default for RemoteHost {
	fn default() -> Self {
		Self::new()
	}
}

impl RemoteHost {
	pub fn new() -> Self {
		Self {
			s: None,
			listen_s: None,
			security: Security::new(),
		}
	}

	pub async fn ready_to_read(self) -> bool {
		if !self.s {
			false
		} else {
			self.s?.ready(Interest::READABLE)
		}
	}

	pub async fn ready_to_write(self) -> bool {
		if !self.s {
			false
		} else {
			self.s?.ready(Interest::WRITABLE)
		}
	}

	pub async fn connect(&mut self, host: &str, port: u16) {
		self.close();
		self.s = Some(
			TcpStream::connect((host, port))
				.await
				.expect("Couldn't connect to the server..."),
		);
		self.security = Security::new();
	}

	pub fn accept(&mut self, socket: TcpStream) {
		self.s = Some(socket);
		self.security = Security::new();
		self.security.generate_security(true, true, true);
	}

	pub async fn recv(&mut self) {
		let mut data = [0u8; MAX_PACKET_SIZE];
		loop {
			self.s.as_ref().unwrap().readable().await.unwrap();
			match self.s.as_ref().unwrap().try_read(&mut data) {
				Ok(0) => break,
				Ok(_n) => {
					self.security.recv(&data);
					let packets = self.security.get_packets_to_recv();
					if packets.is_empty() {
						continue;
					}
					for p in packets {
						// TODO use refactored handle_packet_joymax()
						// if handle_packet_joymax(joymax, silkroad, p) {
						self.security.send(p);
						// }
					}
				}
				Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
					continue;
				}
				Err(e) => {
					eprintln!("failed to read from client socket; err = {:?}", e);
					return; // Err(e.into());
				}
			}
		}
		// Ok(())
	}

	pub async fn write(&mut self) {
		loop {
			self.s.as_ref().unwrap().writable().await.unwrap();
			// See if there are packets to be sent
			let packets = self.security.get_packets_to_send();

			// Send each packet in the list
			for mut p in packets {
				println!("p->s:{:?}", p.get_bytes());
				match self.s.as_ref().unwrap().try_write(&p.get_bytes()) {
					Ok(_) => continue,
					Err(ref e) if e.kind() == ErrorKind::WouldBlock => {
						continue;
					}
					Err(e) => {
						eprintln!("Failed to write to client socket; err = {:?}", e);
						return; // Err(e.into());
					}
				}
			}
		}
	}

	pub async fn send(&mut self, mut data: Vec<u8>) {
		while !data.is_empty() {
			self.s.as_ref().unwrap().writable().await.unwrap();
			let sent = self.s.as_ref().unwrap().try_write(data.as_slice()).unwrap();
			data.drain(0..sent);
		}
	}

	pub fn close(&mut self) {
		if self.s.is_some() {
			self.s = None;
			self.security = Security::new();
		}
	}
}
