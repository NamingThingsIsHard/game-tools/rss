use crate::binary_reader::BinaryReader;
use crate::binary_writer::BinaryWriter;
use crate::packet::Packet;
use crate::packet_data::PacketData;
use crate::proxy::client::LocalHost;
use crate::proxy::server::RemoteHost;
use std::net::{IpAddr, Ipv4Addr, SocketAddr};
// use std::sync::mpsc;
use tokio::io;
use tokio::net::TcpSocket;
// use tokio::sync::oneshot;

/// Settings for a proxy connection.
///
/// # Host types
///
/// A host can be
/// - Client: The silkroad client on your computer.
/// - Proxy: Use to read and manipulate packets, e.g. for 3rd party programs.
/// - Server: The Silkroad server.
/// * Connection can be
///   * Client <-> Proxy
///   * Proxy <-> Server
pub struct Proxy {
	// Replace with your IP address
	bind_ip: String,
	bind_port: u16,

	gateway_host: String,
	gateway_port: u16,

	agent_connect: bool,
	agent_host: String,
	agent_port: u16,
}

impl Default for Proxy {
	fn default() -> Self {
		Self::new()
	}
}

impl Proxy {
	pub fn new() -> Self {
		Self {
			bind_ip: "".to_string(),
			bind_port: 15779,
			gateway_host: "gwgt1.joymax.com".to_string(),
			gateway_port: 15779,

			agent_connect: false,
			agent_host: "".to_string(),
			agent_port: 0,
		}
	}

	pub fn handle_packet_joymax(&mut self, silkroad: &mut LocalHost, packet: Packet) -> bool {
		//print('Joymax\n%s' % packet)

		let mut r = packet.data;

		if packet.opcode == 0xA102 && r.read_uint8() == 1 {
			self.agent_connect = true;

			let login_id = r.read_uint32();

			self.agent_host = r.read_ascii(None).unwrap();
			self.agent_port = r.read_uint16();

			let mut w = PacketData::new();
			w.write_uint8(1);
			w.write_uint32(login_id);
			// w.write_uint16(bind_ip));
			w.write_ascii(&self.bind_ip);
			w.write_uint16(self.bind_port);
			silkroad
				.security
				.send(Packet::new_full(0xA102, w, Some(true), Some(false)));

			return false;
		}

		true
	}

	pub fn handle_packet_silkroad(packet: Packet) -> bool {
		//print('Silkroad\n%s' % packet)

		if packet.opcode == 0x2001 {
			return false;
		}

		true
	}
}

static LOCALHOST: &str = "127.0.0.1";
static BIND_IP: Ipv4Addr = Ipv4Addr::new(127, 0, 0, 1);
static BIND_PORT: u16 = 15779;

fn get_next_open_port() -> u16 {
	let mut test_port = BIND_PORT;
	let mut is_valid_port: bool = false;
	let socket = TcpSocket::new_v4();
	while is_valid_port != false {
		let addr = SocketAddr::new(IpAddr::V4(BIND_IP), BIND_PORT);
		match socket.as_ref().unwrap().bind(addr) {
			Ok(_) => is_valid_port = true,
			Err(_) => {
				test_port += 1;
				continue;
			}
		}
	}
	drop(socket);

	test_port
}

#[tokio::main(flavor = "current_thread")]
pub async fn main() -> io::Result<()> {
	let mut proxy_settings = Proxy::default();
	let mut joymax = RemoteHost::new();
	let mut silkroad = LocalHost::new();

	// let mut joymax_read = RemoteHost::new();
	// let mut joymax_write = RemoteHost::new();
	// let mut silkroad_read = LocalHost::new();
	// let mut silkroad_write = LocalHost::new();
	// let oneshot = oneshot::new(1);

	let bind_port = get_next_open_port();

	// tokio::join!(
	// 	silkroad_read.clone().take().recv(),
	// 	silkroad_write.clone().borrow().write(),
	// 	// joymax.write(),
	// 	// joymax.recv(),
	// );

	loop {
		//TODO add error handling based on https://docs.rs/tokio/latest/tokio/net/struct.TcpStream.html#examples-7
		tokio::select! {
			_ = silkroad.listen(LOCALHOST, bind_port) => {
					if proxy_settings.agent_connect {
						joymax.connect(&proxy_settings.agent_host, proxy_settings.agent_port);
					} else {
						joymax.connect(&proxy_settings.gateway_host, proxy_settings.gateway_port);
					}
				}
			_ = joymax.ready_to_read() => {
				joymax.recv();
				let mut packets = joymax.security.get_packets_to_recv();
				for p in &mut packets {
					if proxy_settings.handle_packet_joymax(&mut silkroad, p) {
						silkroad.security.send(p);
					}
				}
			},
			_ = joymax.ready_to_write() => {
				let &mut packets = joymax.security.get_packets_to_send();
				for p in &mut packets {
					joymax.send(p);
				}
			},
			_ = silkroad.ready_to_read() => {
				silkroad.recv();
				let &mut packets = silkroad.security.get_packets_to_recv();
				for p in &mut packets {
					if Proxy::handle_packet_silkroad(p) {
						joymax.security.send(p);
					}
				}
			},
			_ = silkroad.ready_to_write() => {
				let mut packets = silkroad.security.get_packets_to_send();
				for p in &mut packets {
					silkroad.send(p);
				}
			}

			_ = silkroad.ready_to_read() => {
				println!("terminating accept loop");
			}
		}
	}

	// 	// Errors
	// 	if joymax.s in in_error {
	// 		joymax.close();
	// 	}
	//
	// 	if silkroad.s in in_error {
	// 		silkroad.close();
	// 	}
	//
	// 	sleep(0.01);
	// return 0
}
