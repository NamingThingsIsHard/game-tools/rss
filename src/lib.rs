extern crate core;

mod proxy;
pub mod silkroad_security;

// use pyo3::prelude::*;

pub fn get_package_name() -> &'static str {
	"silkroad-security"
}

pub use crate::silkroad_security::*;

// #[pymodule]
// fn silkroad_security(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
// 	m.add_function(wrap_pyfunction!(silkroad_security, m)?)?;
// 	Ok(())
// }
