use silkroad_security::{
	binary_reader::BinaryReader, binary_seeker::BinarySeeker, binary_writer::BinaryWriter,
	packet::Packet, packet_data::PacketData, security::Security,
};
use std::io::Read;
use std::io::{ErrorKind, Write};
use std::net::TcpStream;
use std::process;

fn handle_packet(security: &mut Security, mut packet: Packet) {
	let mut r: PacketData = PacketData::new_data(packet.get_bytes());

	if packet.opcode == 0x2001 {
		let server: &str = &r.read_ascii(None).unwrap();

		if server == "GatewayServer" {
			println!("GatewayServer");
			let mut w = PacketData::new();
			w.write_uint8(18);
			// w.write_uint16(9);
			w.write_ascii("SR_Client");
			w.write_uint32(432);
			security.send(Packet::new_full(0x6100, w, Some(true), Some(false)));
		}
	} else if packet.opcode == 0xA100 {
		security.send(Packet::new_flags(0x6101, Some(true), None));
	} else if packet.opcode == 0xA101 {
		let mut entry = r.read_uint8();
		println!("[ServerEntries]");
		while entry == 1 {
			r.read_uint8();
			println!("{:?}", r.read_ascii(None).unwrap());
			entry = r.read_uint8();
		}

		entry = r.read_uint8();
		println!("\n[Name]\t\t\tCapacity");
		while entry == 1 {
			let _server_id = r.read_uint16();

			let name = r.read_ascii(None).unwrap();
			let capacity = r.read_float();
			let _state = r.read_uint8();

			println!("[{:?}]\t{:?}", name, capacity as u32);

			entry = r.read_uint8();
		}
		println!("\nExiting..");
		process::exit(1);
	}
}

fn handle_data(stream: &mut TcpStream, security: &mut Security, data: &[u8]) {
	// Packet received
	security.recv(data);

	// See if there are packets that can be processed
	let packets = security.get_packets_to_recv();
	// Iterate each returned packet
	for mut p in packets {
		// Process the packet
		println!("s->p:{:?}", p.get_bytes());
		handle_packet(security, p);
	}

	// See if there are packets to be sent
	let packets = security.get_packets_to_send();

	// Send each packet in the list
	for mut p in packets {
		println!("p->s:{:?}", p.get_bytes());
		let written = stream.write(&p.get_bytes()).expect("Unable to write data");
		if written < p.get_bytes().len() {
			stream.flush().expect("Failed to flush stream.");
		}
	}
}

pub fn request_server_stats() {
	let mut stream =
		TcpStream::connect(("gwgt1.joymax.com", 15779)).expect("Couldn't connect to the server...");

	println!("Successfully connected to server in port 15779");
	stream
		.set_nonblocking(true)
		.expect("set_nonblocking call failed");

	let mut security = Security::new();
	let mut data = [0u8; 8912];
	loop {
		match stream.read(&mut data) {
			Ok(size) => {
				if data.is_empty() {
					println!("Received empty data. Stopping...");
					break;
				}
				if size > 0 {
					println!("data:{:?}", &data[..size]);
				}
				handle_data(&mut stream, &mut security, &data[..size]);
			}
			Err(e) if e.kind() == ErrorKind::WouldBlock => {
				continue; // loop over waiting for the socket
			}
			Err(e) => {
				println!("Failed to receive data: {}", e);
				break;
			}
		};
	}
	println!("Terminated.");
}
