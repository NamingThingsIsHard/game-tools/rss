use crate::silkroad_security::binary_reader::BinaryReader;
use crate::silkroad_security::binary_seeker::BinarySeeker;
use crate::silkroad_security::binary_writer::BinaryWriter;
use byterepr::ByteRepr;
use num::Num;
use std::io::ErrorKind::UnexpectedEof;
use std::io::{Cursor, Error, Read, Seek, SeekFrom, Write};
use std::mem::size_of;
use std::string::FromUtf8Error;

#[derive(Debug, Clone)]
pub struct PacketData {
	data: Cursor<Vec<u8>>,
}

impl Default for PacketData {
	fn default() -> Self {
		Self::new()
	}
}

impl PacketData {
	pub fn new() -> Self {
		Self {
			data: Cursor::new(vec![]),
		}
	}

	pub fn new_data(data: Vec<u8>) -> Self {
		Self {
			data: Cursor::new(data),
		}
	}
}

#[allow(clippy::unused_io_amount)]
impl BinarySeeker for PacketData {
	fn get_bytes(&mut self) -> Vec<u8> {
		self.data.get_ref().clone().to_vec()
	}

	fn get_position(&mut self) -> u64 {
		self.data.position()
	}

	fn get_size(&mut self) -> usize {
		self.data.get_ref().len()
	}

	fn check_offset_in_bounds(&mut self, offset: i64) {
		match offset < 0 || offset > self.data.get_ref().len() as i64 {
			true => panic!(),
			false => (),
		};
	}

	fn seek(&mut self, seek_type: SeekFrom) {
		self.data.seek(seek_type).unwrap();
		self.check_offset_in_bounds(self.data.position() as i64);
	}
	fn seek_forward(&mut self, offset: i64) {
		self.check_offset_in_bounds(self.data.position() as i64 + offset);
		self.data.seek(SeekFrom::Current(offset)).unwrap();
	}
	fn seek_backward(&mut self, offset: i64) {
		self.check_offset_in_bounds(self.data.position() as i64 - offset);
		self.data.seek(SeekFrom::Current(-offset)).unwrap();
	}
	fn seek_set(&mut self, offset: u64) {
		self.check_offset_in_bounds(offset as i64);
		self.data.seek(SeekFrom::Start(offset)).unwrap();
	}
}

impl BinaryReader for PacketData {
	fn reset(&mut self, data: Option<Vec<u8>>, offset: Option<u32>) {
		let data = data.unwrap_or_default();
		self.data = Cursor::new(data);
		self.seek_set(offset.unwrap_or_default() as u64);
	}

	fn bytes_left(&mut self) -> u64 {
		let result = (self.data.get_ref().len() as i64) - self.data.position() as i64;
		if result > 1 {
			result as u64
		} else {
			0
		}
	}

	fn read<T: Num + ByteRepr>(&mut self) -> Result<T, Error> {
		let length: usize = size_of::<T>();
		if self.data.position() + length as u64 > self.data.get_ref().len() as u64 {
			return Err(Error::from(UnexpectedEof));
		}
		let mut buf = vec![0u8; length];
		self.data.read_exact(&mut buf)?;
		Ok(T::from_le_bytes(&buf))
	}

	fn read_array<T: Num + ByteRepr>(&mut self, length: usize) -> Result<Vec<T>, Error> {
		let type_size: usize = size_of::<T>();
		let read_length = length * type_size;
		let read_max = self.data.position() + read_length as u64;
		let stream_len = self.data.get_ref().len() as u64;
		if read_max > stream_len {
			return Err(Error::from(UnexpectedEof));
		}
		let mut bytes: Vec<u8> = vec![0u8; length];
		// TODO test functional implementation
		let read_size = self.data.read(&mut bytes)?;
		if read_size != type_size {
			panic!("Read {:?} instead of expected {:?}", read_size, type_size)
		}

		let result = bytes
			.chunks_exact(type_size)
			.map(|x| T::from_le_bytes(x))
			.collect();
		// for i in 0..length
		// {
		// 	let mut buf = vec![0u8; type_size];
		// 	unpacked = self.data.read_exact(&mut buf)?;
		// 	result[i] = t::from_le_bytes(unpacked);
		// 	self.offset += type_size;
		// }
		Ok(result)
	}

	fn read_string(&mut self, length: usize) -> Result<String, FromUtf8Error> {
		// let type_size: usize = match encoding {
		//	 "utf-16le" => 2,
		//	 "utf-32le" => 4,
		//	 "utf-8" | "ascii" | _ => 1,
		// };
		let mut buf: Vec<u8> = vec![0u8; length];
		self.data.read_exact(&mut buf).unwrap();
		String::from_utf8(buf)
	}

	fn read_int8(&mut self) -> i8 {
		self.read::<i8>().unwrap()
	}

	fn read_uint8(&mut self) -> u8 {
		self.read::<u8>().unwrap()
	}

	fn read_byte(&mut self) -> u8 {
		self.read_uint8()
	}

	fn read_int16(&mut self) -> i16 {
		self.read::<i16>().unwrap()
	}

	fn read_uint16(&mut self) -> u16 {
		self.read::<u16>().unwrap()
	}

	fn read_int32(&mut self) -> i32 {
		self.read::<i32>().unwrap()
	}

	fn read_uint32(&mut self) -> u32 {
		self.read::<u32>().unwrap()
	}

	fn read_int64(&mut self) -> i64 {
		self.read::<i64>().unwrap()
	}

	fn read_uint64(&mut self) -> u64 {
		self.read::<u64>().unwrap()
	}

	fn read_float(&mut self) -> f32 {
		self.read::<f32>().unwrap()
	}

	fn read_double(&mut self) -> f64 {
		self.read::<f64>().unwrap()
	}

	fn read_char(&mut self) -> char {
		self.read::<u8>().unwrap() as char
	}

	fn read_bytes(&mut self, length: usize) -> Vec<u8> {
		self.read_array::<u8>(length).unwrap()
	}

	fn read_ascii(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		let mut length = length.unwrap_or_default();
		if length == 0 {
			length = self.read::<u16>().unwrap();
		}

		self.read_string(usize::from(length))
	}

	fn read_utf8(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		let mut length = length.unwrap_or_default();
		if length == 0 {
			length = self.read::<u16>().unwrap();
		}

		self.read_string(usize::from(length))
		// return s.decode('utf-8')
	}

	fn read_utf16(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		let mut length = length.unwrap_or_default();
		length *= 2;
		if length == 0 {
			length = self.read::<u16>().unwrap();
		}

		self.read_string(usize::from(length))
	}

	fn read_unicode(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		self.read_utf16(length)
	}

	fn read_utf32(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		let mut length = length.unwrap_or_default();
		length *= 4;
		if length == 0 {
			length = self.read::<u16>().unwrap();
		}

		self.read_string(usize::from(length))
	}
}

impl BinaryWriter for PacketData {
	#[allow(clippy::unused_io_amount)]
	fn write(&mut self, val: &[u8]) {
		self.data.write(val).unwrap(); // expect("failed to write {val}");
	}

	#[allow(clippy::unused_io_amount)]
	fn write_string(&mut self, val: &str) {
		let length: u16 = val.len() as u16;
		self.data.write(&length.to_le_bytes()).unwrap();
		self.data.write_all(val.as_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_int8(&mut self, val: i8) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_uint8(&mut self, val: u8) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_byte(&mut self, val: u8) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_int16(&mut self, val: i16) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_uint16(&mut self, val: u16) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_int32(&mut self, val: i32) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_uint32(&mut self, val: u32) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_int64(&mut self, val: i64) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_uint64(&mut self, val: u64) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_float(&mut self, val: f32) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_double(&mut self, val: f64) {
		self.data.write(&val.to_le_bytes()).unwrap();
	}
	#[allow(clippy::unused_io_amount)]
	fn write_char(&mut self, val: char) {
		self.data.write(&[(val as u8)]).unwrap();
	}
	fn write_bytes(&mut self, val: &[u8]) {
		self.data.write_all(val).unwrap();
	}
	fn write_ascii(&mut self, val: &str) {
		self.write_uint16(val.len() as u16);
		self.data.write_all(val.as_bytes()).unwrap();
	}
	fn write_utf8(&mut self, val: &str) {
		self.write_uint16(val.len() as u16);
		self.data.write_all(val.as_bytes()).unwrap();
	}
	fn write_utf16(&mut self, val: &str) {
		self.write_uint16(val.len() as u16);
		self.data.write_all(val.as_bytes()).unwrap();
	}
	fn write_unicode(&mut self, val: &str) {
		self.write_uint16(val.len() as u16);
		self.data.write_all(val.as_bytes()).unwrap();
	}
	fn write_utf32(&mut self, val: &str) {
		self.write_uint16(val.len() as u16);
		self.data.write_all(val.as_bytes()).unwrap();
	}
}

//-----------------------------------------------------------------------------

#[cfg(test)]
mod tests {
	use super::*;
	use crate::silkroad_security::binary_reader::BinaryReader;
	use crate::silkroad_security::packet_data::PacketData;
	use std::fmt::Debug;

	///SeekTests
	#[test]
	#[should_panic]
	fn test_bad_data() {
		let mut packet_data = PacketData::new();
		packet_data.reset(None, Some(1));
	}

	#[test]
	fn test_array_array() {
		let input_array: Vec<u8> = vec![1, 2, 3, 4, 5, 6];
		let mut reader = PacketData::new();
		reader.reset(None, None);
		assert_eq!(reader.data.position(), 0);
		assert_ne!(reader.data.into_inner(), input_array);
	}

	#[test]
	fn test_list() {
		let _list: Option<Vec<u8>> = Some(vec![1, 2, 3, 4, 5, 6]);
		let mut reader = PacketData::new();
		reader.reset(_list.clone(), None);

		assert_eq!(reader.bytes_left(), _list.unwrap().len() as u64);
		assert_eq!(reader.data.position(), 0);
	}

	#[test]
	fn test_bytes_new() {
		let _bytes: Option<Vec<u8>> = Some(vec![1, 2, 3, 4, 5, 6]);
		let mut reader = PacketData::new();
		reader.reset(_bytes.clone(), None);

		assert_eq!(reader.bytes_left(), _bytes.unwrap().len() as u64);
		assert_eq!(reader.data.position(), 0);
	}

	///    Test methods prefixed seek_
	#[test]
	fn test_seek_begin() {
		//Move to position 2
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_set(2);
		assert_eq!(reader.data.position(), 2);
		reader.seek_set(2);
		assert_eq!(reader.data.position(), 2);
	}

	#[test]
	fn test_seek_current() {
		//Move 2 forward
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_forward(2);
		assert_eq!(reader.data.position(), 2);

		reader.seek_forward(2);
		assert_eq!(reader.data.position(), 4);
	}

	#[test]
	fn test_seek_end() {
		//Move 2 backwards from the end
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek(SeekFrom::End(-2));
		assert_eq!(reader.data.position(), 3);

		reader.seek(SeekFrom::End(-1));
		assert_eq!(reader.data.position(), 4);
	}

	#[test]
	#[should_panic]
	fn test_seek_begin_bad_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_set(10);
		// reader.seek_set(-10);
	}

	#[test]
	#[should_panic]
	fn test_seek_current_bad_offset() {
		let mut reader = PacketData::new();
		reader.seek(SeekFrom::Current(-1));
	}

	#[test]
	#[should_panic]
	fn test_seek_end_bad_offset() {
		let mut reader = PacketData::new();
		reader.seek_backward(1);
	}

	#[test]
	#[should_panic]
	fn test_seek_bad_origin() {
		let mut reader = PacketData::new();
		reader.seek(SeekFrom::Current(1));
	}

	#[test]
	fn test_set_good_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		let expected_offset = 3;
		reader.seek_set(expected_offset);
		assert_eq!(reader.data.position(), expected_offset as u64);
	}

	#[test]
	#[should_panic]
	fn test_set_bad_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_set(10);
	}

	#[test]
	fn test_forward_good_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_forward(3);
		assert_eq!(reader.data.position(), 3);
	}

	#[test]
	#[should_panic]
	fn test_forward_bad_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_forward(10);
		reader.seek_forward(-10);
	}

	#[test]
	fn test_backward_good_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_set(4);
		reader.seek_backward(3);
		assert_eq!(reader.data.position(), 1);
	}

	#[test]
	#[should_panic]
	fn test_backward_bad_offset() {
		let mut reader = PacketData::new();
		reader.seek_backward(10);
		reader.seek_backward(-10);
	}

	/// ReadTests

	fn test_read_func<T: Num + ByteRepr + Debug>(
		reader: &mut PacketData,
		expected_read_count: usize,
		expected_value: T,
	) {
		assert_read_is_good::<T>(reader, expected_read_count, expected_value);
		assert_read_is_out_of_bounds::<T>(reader);
	}

	#[should_panic]
	fn assert_read_is_out_of_bounds<T: Num + ByteRepr>(data: &mut PacketData) {
		let result = data.read::<T>();

		assert!(result.is_err())
	}

	fn assert_read_is_good<T: Num + ByteRepr + Debug>(
		reader: &mut PacketData,
		expected_read_count: usize,
		expected_value: T,
	) {
		let old_offset = reader.data.position();
		let result: T = reader.read::<T>().unwrap();

		assert_eq!(expected_value, result);
		assert_eq!(
			reader.data.position(),
			old_offset + expected_read_count as u64
		);
	}

	#[test]
	fn test_int8() {
		let mut reader = PacketData::new_data(vec![50]);
		test_read_func::<i8>(&mut reader, 1, 50);
	}

	#[test]
	fn test_uint8() {
		let mut reader = PacketData::new_data(vec![50]);
		test_read_func::<u8>(&mut reader, 1, 50);
	}

	#[test]
	fn test_byte() {
		let mut reader = PacketData::new_data(vec![128]);
		test_read_func::<u8>(&mut reader, 1, 128);
	}

	#[test]
	fn test_int16() {
		//Max positive value of signed 16bit int
		let mut reader = PacketData::new_data(vec![255, 127]);
		test_read_func::<i16>(&mut reader, 2, i16::MAX);
	}

	#[test]
	fn test_uint16() {
		//Max value of unsigned 16bit int
		let mut reader = PacketData::new_data(vec![255, 255]);
		test_read_func::<u16>(&mut reader, 2, u16::MAX);
	}

	#[test]
	fn test_int32() {
		//Max value of signed 32bit int
		let mut data = vec![255; 3];
		data.push(127);
		let mut reader = PacketData::new_data(data);
		test_read_func::<i32>(&mut reader, 4, i32::MAX);
	}

	#[test]
	fn test_uint32() {
		//Max value of unsigned 32bit int
		let mut reader = PacketData::new_data(vec![255; 4]);
		test_read_func::<u32>(&mut reader, 4, u32::MAX);
	}

	#[test]
	fn test_int64() {
		//Max value of signed 64bit int
		let mut data = vec![255; 7];
		data.push(127);
		let mut reader = PacketData::new_data(data);
		test_read_func::<i64>(&mut reader, 8, i64::MAX);
	}

	#[test]
	fn test_uint64() {
		//Max value of unsigned 64bit int
		let mut reader = PacketData::new_data(vec![255u8; 8]);
		test_read_func::<u64>(&mut reader, 8, u64::MAX);
	}

	#[test]
	fn test_float() {
		let value: f32 = 19.8391;
		let mut reader = PacketData::new_data(value.to_le_bytes().to_vec());
		let old_offset = reader.data.position();

		let read_value = reader.read_float();
		assert_eq!(read_value, value); //, 4);
		assert_eq!(reader.data.position(), old_offset + 4);

		assert_read_is_out_of_bounds::<f32>(&mut reader);
	}

	#[test]
	#[allow(clippy::excessive_precision)]
	fn test_double() {
		let value: f64 = 19.83914711113333344555555;
		let mut reader = PacketData::new_data(value.to_le_bytes().to_vec());
		let old_offset = reader.data.position();

		let read_value = reader.read_double();
		assert_eq!(read_value, value); //, 23);
		assert_eq!(reader.data.position(), old_offset + 8);

		assert_read_is_out_of_bounds::<f64>(&mut reader);
	}

	#[test]
	fn test_char() {
		let mut reader = PacketData::new_data(b"9".to_vec());
		test_read_func::<u8>(&mut reader, 1, b'9');
	}

	#[test]
	#[should_panic]
	fn test_write_uint8() {
		let mut p_d = PacketData::new();
		let val = 0xF;
		p_d.write_uint8(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_uint8());
		let _panic = p_d.read_uint8(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_int8() {
		let mut p_d = PacketData::new();
		let val = 0xF;
		p_d.write_int8(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_int8());
		let _panic = p_d.read_int8(); // should panic
	}

	#[test]
	#[should_panic]
	fn test_write_uint16() {
		let mut p_d = PacketData::new();
		let val = 0xFFFF;
		p_d.write_uint16(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_uint16());
		let _panic = p_d.read_uint16(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_int16() {
		let mut p_d = PacketData::new();
		let val = 0xFFF;
		p_d.write_int16(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_int16());
		let _panic = p_d.read_int16(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_uint32() {
		let mut p_d = PacketData::new();
		let val = 0xFFFFFFF;
		p_d.write_uint32(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_uint32());
		let _panic = p_d.read_uint32(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_int32() {
		let mut p_d = PacketData::new();
		let val = 0xFFFFFFF;
		p_d.write_int32(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_int32());
		let _panic = p_d.read_int32(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_uint64() {
		let mut p_d = PacketData::new();
		let val = 0xFFFFFFFFFFFFFFFF;
		p_d.write_uint64(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_uint64());
		let _panic = p_d.read_uint64(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_int64() {
		let mut p_d = PacketData::new();
		let val = 0xFFFFFFFFFFFFFFF;
		p_d.write_int64(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_int64());
		let _panic = p_d.read_int64(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_float() {
		let mut p_d = PacketData::new();
		let val = 128.128;
		p_d.write_float(val);
		p_d.seek_set(0);
		let val = 128.128;
		p_d.write_float(val);
		let _panic = p_d.read_float(); // should panic
	}

	#[test]
	#[should_panic]
	fn test_write_double() {
		let mut p_d = PacketData::new();
		let val = 256.256;
		p_d.write_double(val);
		p_d.seek_set(0);
		let val = 256.256;
		p_d.write_double(val);
		let _panic = p_d.read_double(); // should panic
	}

	#[test]
	#[should_panic]
	fn test_write_ascii() {
		let mut p_d = PacketData::new();
		let val = "test";
		p_d.write_ascii(val);
		p_d.seek_set(0);
		assert_eq!(val.len() + 2, p_d.get_size());
		assert_eq!(val, p_d.read_ascii(None).unwrap());
		let _panic = p_d.read_ascii(Some(1)); // should panic
	}

	#[test]
	#[should_panic]
	fn test_write_unicode() {
		let mut p_d = PacketData::new();
		let val = "test";
		p_d.write_unicode(val);
		p_d.seek_set(0);
		assert_eq!(val.len() + 2, p_d.get_size());
		assert_eq!(val, p_d.read_unicode(None).unwrap());
		let _panic = p_d.read_unicode(Some(1)); // should panic
	}

	// #[test]
	// #[should_panic]
	// fn test_write_uint_array()
	// {
	// 	let mut p_d = PacketData::new();
	// 	let val = [0];
	// 	p_d.write_uint_array(val);
	// 	p_d.seek_set(0);
	// 	let val = [0];
	// 	p_d.write_uint_array(val);
	// 	let _panic = p_d.read_uint8_array(1);  // should panic
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_uint16_array()
	// {
	// 	let mut p_d = PacketData::new();
	// 	let val = [0xFF];
	// 	p_d.write_uint16_array(val);
	// 	p_d.seek_set(0);
	// 	p_d.read_uint16_array([0xFF]);
	// 	let _panic = p_d.read_uint16_array(1);  // should panic;
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_int16_array()
	// {
	// 	let mut p_d = PacketData::new();
	// 	let val = [0xFF];
	// 	p_d.write_int16_array(val);
	// 	p_d.seek_set(0);
	// 	p_d.read_int16_array([0xFF]);
	// 	let _panic = p_d.read_int16_array(1);  // should panic;
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_uint32_array()
	// {
	// 	let mut p_d = PacketData::new();
	// 	let val = [0xFFFF];
	// 	p_d.write_uint32_array(val);
	// 	p_d.seek_set(0);
	// 	p_d.read_uint32_array([0xFFFF]);
	// 	let _panic = p_d.read_uint32_array(1);  // should panic;
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_int32_array()
	// {
	// 	let mut p_d = PacketData::new();
	//	 let val = [0xFFFF];
	// 	p_d.write_int32_array(val);
	//	 p_d.seek_set(0);
	//	 p_d.read_int32_array([0xFFFF]);
	//	 let _panic = p_d.read_int32_array(1);  // should panic;
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_uint64_array()
	// {
	// 	let mut p_d = PacketData::new();
	//	 let val = [0xFFFFFFFF];
	// 	p_d.write_uint64_array(val);
	//	 p_d.seek_set(0);
	//	 p_d.read_uint64_array([0xFFFFFFFF]);
	//	 let _panic = p_d.read_uint64_array(1);  // should panic;
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_int64_array()
	// {
	// 	let mut p_d = PacketData::new();
	//	 let val = [0xFFFFFFFF];
	// 	p_d.write_int64_array(val);
	//	 p_d.seek_set(0);
	//	 p_d.read_int64_array([0xFFFFFFFF]);
	//	 let _panic = p_d.read_int64_array(1);  // should panic;
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_float_array()
	// {
	// 	let mut p_d = PacketData::new();
	//	 let val = [128];
	// 	p_d.write_float_array(val);
	//	 p_d.seek_set(0);
	//	 let val = [128];
	// 	p_d.write_float_array(val);
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_double_array()
	// {
	// 	let mut p_d = PacketData::new();
	//	 let val = [256.256];
	// 	p_d.write_double_array(val);
	//	 p_d.seek_set(0);
	//	 let val = [256.256];
	// 	p_d.write_double_array(val);
	//	 let _panic = p_d.read_double_array(1);  // should panic;
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_ascii_array()
	// {
	// 	let mut p_d = PacketData::new();
	//	 let val = ["test"];
	// 	p_d.write_ascii_array(val);
	//	 p_d.seek_set(0);
	//	 let val = ["test"];
	// 	p_d.write_ascii_array(val);
	//	 let _panic = p_d.read_ascii_array(1);  // should panic;
	// }
	//
	// #[test]
	// #[should_panic]
	// fn test_write_unicode_array()
	// {
	// 	let mut p_d = PacketData::new();
	//	 let val = ["test"];
	// 	p_d.write_unicode_array(val);
	//	 p_d.seek_set(0);
	//	 p_d.write_unicode_array(["test"]);
	//	 let _panic = p_d.read_unicode_array(1);  // should panic;
	// }
}
