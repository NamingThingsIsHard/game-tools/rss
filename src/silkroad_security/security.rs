#![allow(dead_code, unused_imports)]

use crate::silkroad_security::binary_reader::BinaryReader;
use crate::silkroad_security::binary_seeker::BinarySeeker;
use crate::silkroad_security::binary_writer::BinaryWriter;
use crate::silkroad_security::blowfish::Blowfish;
use crate::silkroad_security::packet::Packet;
use crate::silkroad_security::packet_data::PacketData;
use byterepr::ByteRepr;
use once_cell::sync::Lazy;
use rand::rngs::ThreadRng;
use rand::{thread_rng, RngCore};
use std::fmt::Debug;
use std::num::Wrapping;

#[allow(clippy::redundant_closure)]
pub static GLOBAL_SECURITY_TABLE: Lazy<[u32; 0x10000]> =
	Lazy::new(|| Security::generate_security_table());
pub static BLOWFISH_DEFAULT_KEY: &[u8] = &[0x32, 0xCE, 0xDD, 0x7C, 0xBC, 0xA8];

#[derive(Debug, Copy, Clone)]
pub struct SecurityFlags {
	none: u8,
	blowfish: u8,
	security_bytes: u8,
	handshake: u8,
	handshake_response: u8,
	_6: u8,
	_7: u8,
	_8: u8,
}

impl Default for SecurityFlags {
	fn default() -> Self {
		Self::new()
	}
}

impl SecurityFlags {
	pub fn new() -> Self {
		Self {
			none: 0,
			blowfish: 0,
			security_bytes: 0,
			handshake: 0,
			handshake_response: 0,
			_6: 0,
			_7: 0,
			_8: 0,
		}
	}
}

// //! Returns a byte from a SecurityFlags object.
pub fn from_security_flags(flags: SecurityFlags) -> u8 {
	flags.none
		| flags.blowfish << 1
		| flags.security_bytes << 2
		| flags.handshake << 3
		| flags.handshake_response << 4
		| flags._6 << 5
		| flags._7 << 6
		| flags._8 << 7
}

// //! Returns a SecurityFlags object from a byte.
pub fn to_security_flags(mut value: u8) -> SecurityFlags {
	let mut flags: SecurityFlags = SecurityFlags::new();
	flags.none = value & 1;
	value >>= 1;
	flags.blowfish = value & 1;
	value >>= 1;
	flags.security_bytes = value & 1;
	value >>= 1;
	flags.handshake = value & 1;
	value >>= 1;
	flags.handshake_response = value & 1;
	value >>= 1;
	flags._6 = value & 1;
	value >>= 1;
	flags._7 = value & 1;
	value >>= 1;
	flags._8 = value & 1;

	flags
}

pub struct Security {
	random: ThreadRng,
	m_value_x: u32,
	m_value_g: u32,
	m_value_p: u32,
	m_value_a: u32,
	m_value_b: u32,
	m_value_k: u32,
	m_seed_count: u32,
	m_crc_seed: u32,
	m_initial_blowfish_key: u64,
	m_handshake_blowfish_key: u64,
	m_count_byte_seeds: [u8; 3],
	m_client_key: u64,
	m_challenge_key: u64,

	m_client_security: bool,
	m_security_flag: u8,
	m_security_flags: SecurityFlags,
	m_accepted_handshake: bool,
	m_started_handshake: bool,
	m_identity_flag: u8,
	m_identity_name: String,

	m_incoming_packets: Vec<Packet>,
	m_outgoing_packets: Vec<Packet>,

	m_enc_opcodes: Vec<u16>,

	m_blowfish: Blowfish,

	m_massive_count: u16,
	m_massive_opcode: u16,
	is_m_massive_header: bool,
}

fn make_long_long_(a: u32, b: u32) -> u64 {
	let a_: u32 = a;
	let b_: u32 = b;
	((b_ as u64) << 32) | a_ as u64
}

fn make_long_(a: u16, b: u16) -> u32 {
	let a_: u16 = a;
	let b_: u16 = b;
	((b_ as u32) << 16) | a_ as u32
}

fn make_word_(a: u8, b: u8) -> u16 {
	let a_: u8 = a;
	let b_: u8 = b;
	((b_ as u16) << 8) | a_ as u16
}

fn lo_word_(a: u32) -> u16 {
	(a & 0xFFFF) as u16
}

fn hi_word_(a: u32) -> u16 {
	((a >> 16) & 0xFFFF) as u16
}

fn lo_byte_(a: u16) -> u8 {
	(a & 0xFF) as u8
}

fn hi_byte_(a: u16) -> u8 {
	((a >> 8) & 0xFF) as u8
}

impl Default for Security {
	fn default() -> Self {
		Self::new()
	}
}

impl Security {
	pub fn new() -> Self {
		Self {
			random: thread_rng(),
			m_value_x: 0,
			m_value_g: 0,
			m_value_p: 0,
			m_value_a: 0,
			m_value_b: 0,
			m_value_k: 0,
			m_seed_count: 0,
			m_crc_seed: 0,
			m_initial_blowfish_key: 0,
			m_handshake_blowfish_key: 0,
			m_count_byte_seeds: [0; 3],
			m_client_key: 0,
			m_challenge_key: 0,

			m_client_security: false,
			m_security_flag: 0,
			m_security_flags: SecurityFlags::new(),
			m_accepted_handshake: false,
			m_started_handshake: false,
			m_identity_flag: 0,
			m_identity_name: "SR_Client".to_string(),

			m_outgoing_packets: vec![],
			m_incoming_packets: vec![],

			m_enc_opcodes: vec![0x2001, 0x6100, 0x6101, 0x6102, 0x6103, 0x6107],

			m_blowfish: Blowfish::new(),

			m_massive_count: 0,
			m_massive_opcode: 0,
			is_m_massive_header: false,
		}
	}

	fn generate_security_table() -> [u32; 0x10000] {
		let mut security_table: [u32; 0x10000] = [0; 0x10000];
		let base_security_table: [u8; 1024] = [
			0xB1, 0xD6, 0x8B, 0x96, 0x96, 0x30, 0x07, 0x77, 0x2C, 0x61, 0x0E, 0xEE, 0xBA, 0x51,
			0x09, 0x99, 0x19, 0xC4, 0x6D, 0x07, 0x8F, 0xF4, 0x6A, 0x70, 0x35, 0xA5, 0x63, 0xE9,
			0xA3, 0x95, 0x64, 0x9E, 0x32, 0x88, 0xDB, 0x0E, 0xA4, 0xB8, 0xDC, 0x79, 0x1E, 0xE9,
			0xD5, 0xE0, 0x88, 0xD9, 0xD2, 0x97, 0x2B, 0x4C, 0xB6, 0x09, 0xBD, 0x7C, 0xB1, 0x7E,
			0x07, 0x2D, 0xB8, 0xE7, 0x91, 0x1D, 0xBF, 0x90, 0x64, 0x10, 0xB7, 0x1D, 0xF2, 0x20,
			0xB0, 0x6A, 0x48, 0x71, 0xB1, 0xF3, 0xDE, 0x41, 0xBE, 0x8C, 0x7D, 0xD4, 0xDA, 0x1A,
			0xEB, 0xE4, 0xDD, 0x6D, 0x51, 0xB5, 0xD4, 0xF4, 0xC7, 0x85, 0xD3, 0x83, 0x56, 0x98,
			0x6C, 0x13, 0xC0, 0xA8, 0x6B, 0x64, 0x7A, 0xF9, 0x62, 0xFD, 0xEC, 0xC9, 0x65, 0x8A,
			0x4F, 0x5C, 0x01, 0x14, 0xD9, 0x6C, 0x06, 0x63, 0x63, 0x3D, 0x0F, 0xFA, 0xF5, 0x0D,
			0x08, 0x8D, 0xC8, 0x20, 0x6E, 0x3B, 0x5E, 0x10, 0x69, 0x4C, 0xE4, 0x41, 0x60, 0xD5,
			0x72, 0x71, 0x67, 0xA2, 0xD1, 0xE4, 0x03, 0x3C, 0x47, 0xD4, 0x04, 0x4B, 0xFD, 0x85,
			0x0D, 0xD2, 0x6B, 0xB5, 0x0A, 0xA5, 0xFA, 0xA8, 0xB5, 0x35, 0x6C, 0x98, 0xB2, 0x42,
			0xD6, 0xC9, 0xBB, 0xDB, 0x40, 0xF9, 0xBC, 0xAC, 0xE3, 0x6C, 0xD8, 0x32, 0x75, 0x5C,
			0xDF, 0x45, 0xCF, 0x0D, 0xD6, 0xDC, 0x59, 0x3D, 0xD1, 0xAB, 0xAC, 0x30, 0xD9, 0x26,
			0x3A, 0x00, 0xDE, 0x51, 0x80, 0x51, 0xD7, 0xC8, 0x16, 0x61, 0xD0, 0xBF, 0xB5, 0xF4,
			0xB4, 0x21, 0x23, 0xC4, 0xB3, 0x56, 0x99, 0x95, 0xBA, 0xCF, 0x0F, 0xA5, 0xB7, 0xB8,
			0x9E, 0xB8, 0x02, 0x28, 0x08, 0x88, 0x05, 0x5F, 0xB2, 0xD9, 0xEC, 0xC6, 0x24, 0xE9,
			0x0B, 0xB1, 0x87, 0x7C, 0x6F, 0x2F, 0x11, 0x4C, 0x68, 0x58, 0xAB, 0x1D, 0x61, 0xC1,
			0x3D, 0x2D, 0x66, 0xB6, 0x90, 0x41, 0xDC, 0x76, 0x06, 0x71, 0xDB, 0x01, 0xBC, 0x20,
			0xD2, 0x98, 0x2A, 0x10, 0xD5, 0xEF, 0x89, 0x85, 0xB1, 0x71, 0x1F, 0xB5, 0xB6, 0x06,
			0xA5, 0xE4, 0xBF, 0x9F, 0x33, 0xD4, 0xB8, 0xE8, 0xA2, 0xC9, 0x07, 0x78, 0x34, 0xF9,
			0xA0, 0x0F, 0x8E, 0xA8, 0x09, 0x96, 0x18, 0x98, 0x0E, 0xE1, 0xBB, 0x0D, 0x6A, 0x7F,
			0x2D, 0x3D, 0x6D, 0x08, 0x97, 0x6C, 0x64, 0x91, 0x01, 0x5C, 0x63, 0xE6, 0xF4, 0x51,
			0x6B, 0x6B, 0x62, 0x61, 0x6C, 0x1C, 0xD8, 0x30, 0x65, 0x85, 0x4E, 0x00, 0x62, 0xF2,
			0xED, 0x95, 0x06, 0x6C, 0x7B, 0xA5, 0x01, 0x1B, 0xC1, 0xF4, 0x08, 0x82, 0x57, 0xC4,
			0x0F, 0xF5, 0xC6, 0xD9, 0xB0, 0x63, 0x50, 0xE9, 0xB7, 0x12, 0xEA, 0xB8, 0xBE, 0x8B,
			0x7C, 0x88, 0xB9, 0xFC, 0xDF, 0x1D, 0xDD, 0x62, 0x49, 0x2D, 0xDA, 0x15, 0xF3, 0x7C,
			0xD3, 0x8C, 0x65, 0x4C, 0xD4, 0xFB, 0x58, 0x61, 0xB2, 0x4D, 0xCE, 0x51, 0xB5, 0x3A,
			0x74, 0x00, 0xBC, 0xA3, 0xE2, 0x30, 0xBB, 0xD4, 0x41, 0xA5, 0xDF, 0x4A, 0xD7, 0x95,
			0xD8, 0x3D, 0x6D, 0xC4, 0xD1, 0xA4, 0xFB, 0xF4, 0xD6, 0xD3, 0x6A, 0xE9, 0x69, 0x43,
			0xFC, 0xD9, 0x6E, 0x34, 0x46, 0x88, 0x67, 0xAD, 0xD0, 0xB8, 0x60, 0xDA, 0x73, 0x2D,
			0x04, 0x44, 0xE5, 0x1D, 0x03, 0x33, 0x5F, 0x4C, 0x0A, 0xAA, 0xC9, 0x7C, 0x0D, 0xDD,
			0x3C, 0x71, 0x05, 0x50, 0xAA, 0x41, 0x02, 0x27, 0x10, 0x10, 0x0B, 0xBE, 0x86, 0x20,
			0x0C, 0xC9, 0x25, 0xB5, 0x68, 0x57, 0xB3, 0x85, 0x6F, 0x20, 0x09, 0xD4, 0x66, 0xB9,
			0x9F, 0xE4, 0x61, 0xCE, 0x0E, 0xF9, 0xDE, 0x5E, 0x08, 0xC9, 0xD9, 0x29, 0x22, 0x98,
			0xD0, 0xB0, 0xB4, 0xA8, 0x57, 0xC7, 0x17, 0x3D, 0xB3, 0x59, 0x81, 0x0D, 0xB4, 0x3E,
			0x3B, 0x5C, 0xBD, 0xB7, 0xAD, 0x6C, 0xBA, 0xC0, 0x20, 0x83, 0xB8, 0xED, 0xB6, 0xB3,
			0xBF, 0x9A, 0x0C, 0xE2, 0xB6, 0x03, 0x9A, 0xD2, 0xB1, 0x74, 0x39, 0x47, 0xD5, 0xEA,
			0xAF, 0x77, 0xD2, 0x9D, 0x15, 0x26, 0xDB, 0x04, 0x83, 0x16, 0xDC, 0x73, 0x12, 0x0B,
			0x63, 0xE3, 0x84, 0x3B, 0x64, 0x94, 0x3E, 0x6A, 0x6D, 0x0D, 0xA8, 0x5A, 0x6A, 0x7A,
			0x0B, 0xCF, 0x0E, 0xE4, 0x9D, 0xFF, 0x09, 0x93, 0x27, 0xAE, 0x00, 0x0A, 0xB1, 0x9E,
			0x07, 0x7D, 0x44, 0x93, 0x0F, 0xF0, 0xD2, 0xA2, 0x08, 0x87, 0x68, 0xF2, 0x01, 0x1E,
			0xFE, 0xC2, 0x06, 0x69, 0x5D, 0x57, 0x62, 0xF7, 0xCB, 0x67, 0x65, 0x80, 0x71, 0x36,
			0x6C, 0x19, 0xE7, 0x06, 0x6B, 0x6E, 0x76, 0x1B, 0xD4, 0xFE, 0xE0, 0x2B, 0xD3, 0x89,
			0x5A, 0x7A, 0xDA, 0x10, 0xCC, 0x4A, 0xDD, 0x67, 0x6F, 0xDF, 0xB9, 0xF9, 0xF9, 0xEF,
			0xBE, 0x8E, 0x43, 0xBE, 0xB7, 0x17, 0xD5, 0x8E, 0xB0, 0x60, 0xE8, 0xA3, 0xD6, 0xD6,
			0x7E, 0x93, 0xD1, 0xA1, 0xC4, 0xC2, 0xD8, 0x38, 0x52, 0xF2, 0xDF, 0x4F, 0xF1, 0x67,
			0xBB, 0xD1, 0x67, 0x57, 0xBC, 0xA6, 0xDD, 0x06, 0xB5, 0x3F, 0x4B, 0x36, 0xB2, 0x48,
			0xDA, 0x2B, 0x0D, 0xD8, 0x4C, 0x1B, 0x0A, 0xAF, 0xF6, 0x4A, 0x03, 0x36, 0x60, 0x7A,
			0x04, 0x41, 0xC3, 0xEF, 0x60, 0xDF, 0x55, 0xDF, 0x67, 0xA8, 0xEF, 0x8E, 0x6E, 0x31,
			0x79, 0x0E, 0x69, 0x46, 0x8C, 0xB3, 0x51, 0xCB, 0x1A, 0x83, 0x63, 0xBC, 0xA0, 0xD2,
			0x6F, 0x25, 0x36, 0xE2, 0x68, 0x52, 0x95, 0x77, 0x0C, 0xCC, 0x03, 0x47, 0x0B, 0xBB,
			0xB9, 0x14, 0x02, 0x22, 0x2F, 0x26, 0x05, 0x55, 0xBE, 0x3B, 0xB6, 0xC5, 0x28, 0x0B,
			0xBD, 0xB2, 0x92, 0x5A, 0xB4, 0x2B, 0x04, 0x6A, 0xB3, 0x5C, 0xA7, 0xFF, 0xD7, 0xC2,
			0x31, 0xCF, 0xD0, 0xB5, 0x8B, 0x9E, 0xD9, 0x2C, 0x1D, 0xAE, 0xDE, 0x5B, 0xB0, 0x72,
			0x64, 0x9B, 0x26, 0xF2, 0xE3, 0xEC, 0x9C, 0xA3, 0x6A, 0x75, 0x0A, 0x93, 0x6D, 0x02,
			0xA9, 0x06, 0x09, 0x9C, 0x3F, 0x36, 0x0E, 0xEB, 0x85, 0x68, 0x07, 0x72, 0x13, 0x07,
			0x00, 0x05, 0x82, 0x48, 0xBF, 0x95, 0x14, 0x7A, 0xB8, 0xE2, 0xAE, 0x2B, 0xB1, 0x7B,
			0x38, 0x1B, 0xB6, 0x0C, 0x9B, 0x8E, 0xD2, 0x92, 0x0D, 0xBE, 0xD5, 0xE5, 0xB7, 0xEF,
			0xDC, 0x7C, 0x21, 0xDF, 0xDB, 0x0B, 0x94, 0xD2, 0xD3, 0x86, 0x42, 0xE2, 0xD4, 0xF1,
			0xF8, 0xB3, 0xDD, 0x68, 0x6E, 0x83, 0xDA, 0x1F, 0xCD, 0x16, 0xBE, 0x81, 0x5B, 0x26,
			0xB9, 0xF6, 0xE1, 0x77, 0xB0, 0x6F, 0x77, 0x47, 0xB7, 0x18, 0xE0, 0x5A, 0x08, 0x88,
			0x70, 0x6A, 0x0F, 0xF1, 0xCA, 0x3B, 0x06, 0x66, 0x5C, 0x0B, 0x01, 0x11, 0xFF, 0x9E,
			0x65, 0x8F, 0x69, 0xAE, 0x62, 0xF8, 0xD3, 0xFF, 0x6B, 0x61, 0x45, 0xCF, 0x6C, 0x16,
			0x78, 0xE2, 0x0A, 0xA0, 0xEE, 0xD2, 0x0D, 0xD7, 0x54, 0x83, 0x04, 0x4E, 0xC2, 0xB3,
			0x03, 0x39, 0x61, 0x26, 0x67, 0xA7, 0xF7, 0x16, 0x60, 0xD0, 0x4D, 0x47, 0x69, 0x49,
			0xDB, 0x77, 0x6E, 0x3E, 0x4A, 0x6A, 0xD1, 0xAE, 0xDC, 0x5A, 0xD6, 0xD9, 0x66, 0x0B,
			0xDF, 0x40, 0xF0, 0x3B, 0xD8, 0x37, 0x53, 0xAE, 0xBC, 0xA9, 0xC5, 0x9E, 0xBB, 0xDE,
			0x7F, 0xCF, 0xB2, 0x47, 0xE9, 0xFF, 0xB5, 0x30, 0x1C, 0xF9, 0xBD, 0xBD, 0x8A, 0xCD,
			0xBA, 0xCA, 0x30, 0x9E, 0xB3, 0x53, 0xA6, 0xA3, 0xBC, 0x24, 0x05, 0x3B, 0xD0, 0xBA,
			0xA3, 0x06, 0xD7, 0xCD, 0xE9, 0x57, 0xDE, 0x54, 0xBF, 0x67, 0xD9, 0x23, 0x2E, 0x72,
			0x66, 0xB3, 0xB8, 0x4A, 0x61, 0xC4, 0x02, 0x1B, 0x38, 0x5D, 0x94, 0x2B, 0x6F, 0x2B,
			0x37, 0xBE, 0xCB, 0xB4, 0xA1, 0x8E, 0xCC, 0xC3, 0x1B, 0xDF, 0x0D, 0x5A, 0x8D, 0xED,
			0x02, 0x2D,
		];

		let mut reader = PacketData::new_data(base_security_table.to_vec());
		let mut index: usize = 0;
		for _edi in (0..1024).step_by(4) {
			let edx = reader.read_uint32();
			for ecx in 0..256 {
				let mut eax = ecx >> 1;
				if (ecx & 1) != 0 {
					eax ^= edx;
				}
				for _bit in 0..7 {
					if (eax & 1) != 0 {
						eax >>= 1;
						eax ^= edx;
					} else {
						eax >>= 1;
					}
				}
				security_table[index] = eax;
				index += 1;
			}
		}

		security_table
	}

	// //! Use one security table for all objects.

	fn next_uint64(&mut self) -> u64 {
		self.random.next_u64()
	}

	fn next_uint32(&mut self) -> u32 {
		self.random.next_u32()
	}

	fn next_uint16(&mut self) -> u16 {
		(self.random.next_u32() & 0xFFFF) as u16
	}

	fn next_uint8(&mut self) -> u8 {
		(thread_rng().next_u32() & 0xFF) as u8
	}

	// This function's logic was written by jMerlin as part of the article "How to generate the security bytes for SRO"
	fn generate_value(val: &mut u32) -> u32 {
		for _i in 0..32 {
			*val = (((((((((((*val >> 2) ^ *val) >> 2) ^ *val) >> 1) ^ *val) >> 1) ^ *val) >> 1)
				^ *val) & 1) | ((((*val & 1) << 31) | (*val >> 1)) & 0xFFFFFFFE);
		}
		*val
	}

	// Sets up the count bytes
	// This function's logic was written by jMerlin as part of the article "How to generate the security bytes for SRO"
	fn setup_count_byte(&mut self, mut seed: u32) {
		if seed == 0 {
			seed = 0x9ABFB3B6;
		}
		let mut mut0: u32 = seed;
		let mut1: u32 = Security::generate_value(&mut mut0);
		let mut2: u32 = Security::generate_value(&mut mut0);
		let mut3: u32 = Security::generate_value(&mut mut0);
		Security::generate_value(&mut mut0);
		let mut byte1: u8 = ((mut0 & 0xFF) ^ (mut3 & 0xFF)) as u8;
		let mut byte2: u8 = ((mut1 & 0xFF) ^ (mut2 & 0xFF)) as u8;
		if byte1 == 0 {
			byte1 = 1;
		}
		if byte2 == 0 {
			byte2 = 1;
		}
		self.m_count_byte_seeds[0] = byte1 ^ byte2;
		self.m_count_byte_seeds[1] = byte2;
		self.m_count_byte_seeds[2] = byte1;
	}

	// Helper function used in the handshake, X may be a or b, this clean version of the function is from jMerlin (Func_X_4)
	fn g_pow_x_mod_p(&mut self, p: u32, mut x: u32, g: u32) -> u32 {
		let mut result: i64 = 1;
		let mut mult: i64 = i64::from(g);
		if x == 0 {
			return 1;
		}
		while x != 0 {
			if (x & 1) > 0 {
				result = (mult * result) % p as i64;
			}
			x >>= 1;
			mult = (mult * mult) % p as i64;
		}
		result as u32
	}

	// Helper function used in the handshake (Func_X_2)
	fn key_transform_value(val: &mut u64, key: u32, key_byte: u8) {
		let mut stream = val.to_le_bytes();
		stream[0] ^= u8::wrapping_add(
			stream[0],
			u8::wrapping_add(lo_byte_(lo_word_(key)), key_byte),
		);
		stream[1] ^= u8::wrapping_add(
			stream[1],
			u8::wrapping_add(hi_byte_(lo_word_(key)), key_byte),
		);
		stream[2] ^= u8::wrapping_add(
			stream[2],
			u8::wrapping_add(lo_byte_(hi_word_(key)), key_byte),
		);
		stream[3] ^= u8::wrapping_add(
			stream[3],
			u8::wrapping_add(hi_byte_(hi_word_(key)), key_byte),
		);
		stream[4] ^= u8::wrapping_add(
			stream[4],
			u8::wrapping_add(lo_byte_(lo_word_(key)), key_byte),
		);
		stream[5] ^= u8::wrapping_add(
			stream[5],
			u8::wrapping_add(hi_byte_(lo_word_(key)), key_byte),
		);
		stream[6] ^= u8::wrapping_add(
			stream[6],
			u8::wrapping_add(lo_byte_(hi_word_(key)), key_byte),
		);
		stream[7] ^= u8::wrapping_add(
			stream[7],
			u8::wrapping_add(hi_byte_(hi_word_(key)), key_byte),
		);
		*val = u64::from_le_bytes(stream);
	}

	// Function called to generate a count byte
	// This function's logic was written by jMerlin as part of the article "How to generate the security bytes for SRO"
	fn generate_count_byte(&mut self, update: bool) -> u8 {
		let mut result: u8 = u8::wrapping_mul(
			self.m_count_byte_seeds[2],
			u8::wrapping_add(!self.m_count_byte_seeds[0], self.m_count_byte_seeds[1]),
		);
		result = result ^ (result >> 4);
		if update {
			self.m_count_byte_seeds[0] = result;
		}
		result
	}

	// Function called to generate the crc byte
	// This function's logic was written by jMerlin as part of the article "How to generate the security bytes for SRO"
	fn generate_check_byte(&self, stream: &[u8], offset: i32, length: Option<usize>) -> u8 {
		let length = length.unwrap_or(stream.len());
		let mut checksum: u32 = 0xFFFFFFFF;
		let modded_seed: u32 = self.m_crc_seed << 8;
		for x in (offset as usize)..(offset as usize + length as usize) {
			checksum = (checksum >> 8)
				^ GLOBAL_SECURITY_TABLE
					[(modded_seed + ((u32::from(stream[x as usize]) ^ checksum) & 0xFF)) as usize];
		}
		(((checksum >> 24) & 0xFF)
			+ ((checksum >> 8) & 0xFF)
			+ ((checksum >> 16) & 0xFF)
			+ (checksum & 0xFF)) as u8
	}

	pub fn generate_security_from_flags(&mut self, flags: SecurityFlags) {
		self.m_security_flag = from_security_flags(flags);
		self.m_security_flags = flags;
		self.m_client_security = true;

		let mut response: Packet = Packet::new(0x5000);

		response.write_uint8(self.m_security_flag);

		if self.m_security_flags.blowfish == 1 {
			self.m_initial_blowfish_key = self.next_uint64();

			self.m_blowfish
				.initialize(&mut self.m_initial_blowfish_key.to_le_bytes());

			response.write_uint64(self.m_initial_blowfish_key);
		}
		if self.m_security_flags.security_bytes == 1 {
			self.m_seed_count = self.next_uint8() as u32;
			self.setup_count_byte(self.m_seed_count);
			self.m_crc_seed = self.next_uint8() as u32;

			response.write_uint32(self.m_seed_count);
			response.write_uint32(self.m_crc_seed);
		}
		if self.m_security_flags.handshake == 1 {
			self.m_handshake_blowfish_key = self.next_uint64();
			self.m_value_x = self.next_uint32() & 0x7FFFFFFF;
			self.m_value_g = self.next_uint32() & 0x7FFFFFFF;
			self.m_value_p = self.next_uint32() & 0x7FFFFFFF;
			self.m_value_a = self.g_pow_x_mod_p(self.m_value_p, self.m_value_x, self.m_value_g);

			response.write_uint64(self.m_handshake_blowfish_key);
			response.write_uint32(self.m_value_g);
			response.write_uint32(self.m_value_p);
			response.write_uint32(self.m_value_a);
		}

		self.m_outgoing_packets.push(response);
	}

	fn handshake(
		&mut self,
		packet_opcode: u16,
		mut packet_data: PacketData,
		packet_encrypted: bool,
	) {
		if packet_encrypted {
			panic!("[SecurityAPI::handshake] Received an illogical (encrypted) handshake packet.");
		}
		if self.m_client_security {
			// If this object does not need a handshake
			if self.m_security_flags.handshake == 0 {
				// Client should only accept it then
				if packet_opcode == 0x9000 {
					if self.m_accepted_handshake {
						panic!("[SecurityAPI::handshake] Received an illogical handshake packet (duplicate 0x9000).");
					}
					self.m_accepted_handshake = true; // Otherwise, all good here
					return;
				}
				// Client should not send any 0x5000s!
				else if packet_opcode == 0x5000 {
					panic!("[SecurityAPI::handshake] Received an illogical handshake packet (0x5000 with no handshake).");
				}
				// Programmer made a mistake in calling this function
				else {
					panic!("[SecurityAPI::handshake] Received an illogical handshake packet (programmer error).");
				}
			} else {
				// Client accepts the handshake
				if packet_opcode == 0x9000 {
					// Can't accept it before it's started!
					if !self.m_started_handshake {
						panic!("[SecurityAPI::handshake] Received an illogical handshake packet (out of order 0x9000).");
					}
					if self.m_accepted_handshake
					// Client error
					{
						panic!("[SecurityAPI::handshake] Received an illogical handshake packet (duplicate 0x9000).");
					}
					// Otherwise, all good here
					self.m_accepted_handshake = true;
					return;
				}
				// Client sends a handshake response
				else if packet_opcode == 0x5000 {
					if self.m_started_handshake
					// Client error
					{
						panic!("[SecurityAPI::handshake] Received an illogical handshake packet (duplicate 0x5000).");
					}
					self.m_started_handshake = true;
				}
				// Programmer made a mistake in calling this function
				else {
					panic!("[SecurityAPI::handshake] Received an illogical handshake packet (programmer error).");
				}
			}

			let mut key_array: u64;
			let mut tmp_bytes: &[u8];

			self.m_value_b = packet_data.read_uint32();
			self.m_client_key = packet_data.read_uint64();

			self.m_value_k = self.g_pow_x_mod_p(self.m_value_p, self.m_value_x, self.m_value_b);

			key_array = make_long_long_(self.m_value_a, self.m_value_b);
			Security::key_transform_value(
				&mut key_array,
				self.m_value_k,
				lo_byte_(lo_word_(self.m_value_k)) & 0x03,
			);
			self.m_blowfish
				.initialize(&mut key_array.to_le_bytes()[0..8]);

			let decoded = self
				.m_blowfish
				.decode(self.m_client_key.into_le_bytes().as_slice(), 0)
				.unwrap();
			tmp_bytes = decoded.as_slice();
			self.m_client_key = u64::try_from_le_bytes(tmp_bytes).unwrap();

			key_array = make_long_long_(self.m_value_b, self.m_value_a);
			Security::key_transform_value(
				&mut key_array,
				self.m_value_k,
				lo_byte_(lo_word_(self.m_value_b)) & 0x07,
			);
			if self.m_client_key != key_array {
				panic!("[SecurityAPI::handshake] Client signature error.");
			}

			key_array = make_long_long_(self.m_value_a, self.m_value_b);
			Security::key_transform_value(
				&mut key_array,
				self.m_value_k,
				lo_byte_(lo_word_(self.m_value_k)) & 0x03,
			);
			self.m_blowfish
				.initialize(key_array.into_le_bytes().as_mut_slice());

			self.m_challenge_key = make_long_long_(self.m_value_a, self.m_value_b);
			Security::key_transform_value(
				&mut self.m_challenge_key,
				self.m_value_k,
				lo_byte_(lo_word_(self.m_value_a)) & 0x07,
			);
			let encoded = &self
				.m_blowfish
				.encode(self.m_challenge_key.into_le_bytes().as_mut_slice(), 0, 8)
				.unwrap();
			tmp_bytes = encoded.as_slice();
			self.m_challenge_key = u64::try_from_le_bytes(tmp_bytes).unwrap();

			Security::key_transform_value(&mut self.m_handshake_blowfish_key, self.m_value_k, 0x3);
			self.m_blowfish
				.initialize(self.m_handshake_blowfish_key.into_le_bytes().as_mut_slice());

			let mut tmp_flags: SecurityFlags = SecurityFlags::new();
			tmp_flags.handshake_response = 1;
			let tmp_flag: u8 = from_security_flags(tmp_flags);

			let mut response: Packet = Packet::new(0x5000);
			response.write_uint8(tmp_flag);
			response.write_uint64(self.m_challenge_key);
			self.m_outgoing_packets.push(response);
		} else {
			if packet_opcode != 0x5000 {
				panic!("[SecurityAPI::handshake] Received an illogical handshake packet (programmer error).");
			}

			let flag: u8 = packet_data.read_byte();

			let flags: SecurityFlags = to_security_flags(flag);

			if self.m_security_flag == 0 {
				self.m_security_flag = flag;
				self.m_security_flags = flags;
			}

			if flags.blowfish == 1 {
				self.m_initial_blowfish_key = packet_data.read_uint64();
				self.m_blowfish
					.initialize(self.m_initial_blowfish_key.into_le_bytes().as_mut_slice());
			}

			if flags.security_bytes == 1 {
				self.m_seed_count = packet_data.read_uint32();
				self.m_crc_seed = packet_data.read_uint32();
				self.setup_count_byte(self.m_seed_count);
			}

			if flags.handshake == 1 {
				self.m_handshake_blowfish_key = packet_data.read_uint64();
				self.m_value_g = packet_data.read_uint32();
				self.m_value_p = packet_data.read_uint32();
				self.m_value_a = packet_data.read_uint32();

				self.m_value_x = self.next_uint32() & 0x7FFFFFFF;

				self.m_value_b = self.g_pow_x_mod_p(self.m_value_p, self.m_value_x, self.m_value_g);
				self.m_value_k = self.g_pow_x_mod_p(self.m_value_p, self.m_value_x, self.m_value_a);

				let mut key_array: u64 = make_long_long_(self.m_value_a, self.m_value_b);
				Security::key_transform_value(
					&mut key_array,
					self.m_value_k,
					lo_byte_(lo_word_(self.m_value_k)) & 0x03,
				);
				self.m_blowfish
					.initialize(key_array.into_le_bytes().as_mut_slice());

				self.m_client_key = make_long_long_(self.m_value_b, self.m_value_a);
				Security::key_transform_value(
					&mut self.m_client_key,
					self.m_value_k,
					lo_byte_(lo_word_(self.m_value_b)) & 0x07,
				);
				let tmp_bytes: &[u8] = &self
					.m_blowfish
					.encode(self.m_client_key.into_le_bytes().as_mut_slice(), 0, 8)
					.unwrap();
				self.m_client_key = u64::try_from_le_bytes(tmp_bytes).unwrap();
			}

			if flags.handshake_response == 1 {
				self.m_challenge_key = packet_data.read_uint64();

				let mut expected_challenge_key: u64 =
					make_long_long_(self.m_value_a, self.m_value_b);
				Security::key_transform_value(
					&mut expected_challenge_key,
					self.m_value_k,
					lo_byte_(lo_word_(self.m_value_a)) & 0x07,
				);
				let tmp_bytes: &[u8] = &self
					.m_blowfish
					.encode(expected_challenge_key.into_le_bytes().as_mut_slice(), 0, 8)
					.unwrap();
				expected_challenge_key = u64::try_from_le_bytes(tmp_bytes).unwrap();

				if self.m_challenge_key != expected_challenge_key {
					panic!("[SecurityAPI::handshake] Server signature error.");
				}

				Security::key_transform_value(
					&mut self.m_handshake_blowfish_key,
					self.m_value_k,
					0x3,
				);
				self.m_blowfish
					.initialize(self.m_handshake_blowfish_key.into_le_bytes().as_mut_slice());
			}

			// Generate the outgoing packet now
			if flags.handshake == 1 && flags.handshake_response == 0 {
				// Check to see if we already started a handshake
				if self.m_started_handshake || self.m_accepted_handshake {
					panic!("[SecurityAPI::handshake] Received an illogical handshake packet (duplicate 0x5000).");
				}

				// handshake challenge
				let mut response: Packet = Packet::new(0x5000);
				response.write_uint32(self.m_value_b);
				response.write_uint64(self.m_client_key);
				self.m_outgoing_packets.insert(0, response);

				// The handshake has started
				self.m_started_handshake = true;
			} else {
				// Check to see if we already accepted a handshake
				if self.m_accepted_handshake {
					panic!("[SecurityAPI::handshake] Received an illogical handshake packet (duplicate 0x5000).");
				}

				// handshake accepted
				let response1: Packet = Packet::new(0x9000);

				// Identify
				let mut response2: Packet = Packet::new_flags(0x2001, Some(true), Some(false));
				response2.write_ascii(&self.m_identity_name);
				response2.write_uint8(self.m_identity_flag);

				// Insert at the front, we want 0x9000 first, then 0x2001
				self.m_outgoing_packets.push(response2);
				self.m_outgoing_packets.push(response1);

				// Mark the handshake as accepted now
				self.m_started_handshake = true;
				self.m_accepted_handshake = true;
			}
		}
	}

	fn format_packet(&mut self, opcode: u16, data: &[u8], encrypted: bool) -> Vec<u8> {
		// Sanity check
		if data.len() >= 0x8000 {
			panic!("[SecurityAPI::format_packet] Payload is too large!");
		}

		let data_length: u16 = data.len() as u16;

		// Add the packet header to the start of the data
		let mut writer: PacketData = PacketData::new();
		writer.write_uint16(data_length); // packet size
		writer.write_uint16(opcode); // packet opcode
		writer.write_uint16(0); // packet security bytes
		writer.write_bytes(data);

		// Determine if we need to mark the packet size as encrypted
		if encrypted
			&& (self.m_security_flags.blowfish == 1
				|| (self.m_security_flags.security_bytes == 1
					&& self.m_security_flags.blowfish == 0))
		{
			let seek_index: u64 = writer.clone().get_bytes().as_mut_slice().len() as u64;

			let packet_size: u16 = ((writer.get_size() - 6) | 0x8000) as u16;
			writer.seek_set(0);
			writer.write_uint16(packet_size as u16);

			writer.seek_set(seek_index);
		}

		// Only need to stamp bytes if this is a clientless object
		if !self.m_client_security && self.m_security_flags.security_bytes == 1 {
			let seek_index: u64 = writer.clone().get_bytes().len() as u64;

			let sb1: u8 = self.generate_count_byte(true);
			writer.seek_set(4);
			writer.write_uint8(sb1);

			let sb2: u8 = self.generate_check_byte(&writer.clone().get_bytes(), 0, None);
			writer.seek_set(5);
			writer.write_uint8(sb2);

			writer.seek_set(seek_index as u64);
		}

		// If the packet should be physically encrypted, return an encrypted version of it
		if encrypted && self.m_security_flags.blowfish == 1 {
			let raw_data: &[u8] = &writer.clone().get_bytes();
			let encrypted_data: &[u8] = &mut self
				.m_blowfish
				.encode(raw_data, 2, raw_data.len() - 2)
				.unwrap();

			writer.seek_set(2);

			writer.write(encrypted_data);
		} else {
			// Determine if we need to unmark the packet size from being encrypted but not physically encrypted
			if encrypted
				&& (self.m_security_flags.security_bytes == 1
					&& self.m_security_flags.blowfish == 0)
			{
				let seek_index: u64 = writer.clone().get_bytes().len() as u64;

				writer.seek_set(0);
				writer.write_uint16(data_length);
				// writer.flush();

				writer.seek_set(seek_index);
			}
		}

		// Return the final data
		writer.get_bytes()
	}

	fn has_packet_to_send(&mut self) -> bool {
		// No packets, easy case
		if self.m_outgoing_packets.is_empty() {
			return false;
		}

		// If we have packets and have accepted the handshake, we can send whenever,
		// so return true.
		if self.m_accepted_handshake {
			return true;
		}

		// Otherwise, check to see if we have pending handshake packets to send
		let packet: Packet = self.m_outgoing_packets[0].clone();
		if packet.opcode == 0x5000 || packet.opcode == 0x9000 {
			return true;
		}

		// If we get here, we have out of order packets that cannot be sent yet.
		false
	}

	// Returns a list of packets that is ready to be sent. These buffers must be sent in order.
	pub fn get_packets_to_send(&mut self) -> Vec<PacketData> {
		let mut packets: Vec<PacketData> = vec![];
		critical_section::with(|_cs| {
			while self.has_packet_to_send() {
				packets.push(PacketData::new_data(self.get_packet_to_send().to_vec()));
			}
		});
		packets
	}

	// Returns a list of all packets that are ready for processing.
	pub fn get_packets_to_recv(&mut self) -> Vec<Packet> {
		let mut packets: Vec<Packet> = vec![];
		critical_section::with(|_cs| {
			if !self.m_incoming_packets.is_empty() {
				packets = self.m_incoming_packets.clone();
				self.m_incoming_packets = vec![];
			}
		});
		packets
	}

	fn get_packet_to_send(&mut self) -> Vec<u8> {
		if self.m_outgoing_packets.is_empty() {
			panic!("[SilkroadSecurityData::get_packet_to_send] No packets are available to send.");
		}

		let mut packet_container: Packet = self.m_outgoing_packets.pop().unwrap();

		if packet_container.massive {
			// let mut workspace: [u8; 4089] = [0; 4089];
			let mut parts: usize = 0;

			let mut final_packet: PacketData = PacketData::new();
			let mut final_data: PacketData = PacketData::new();

			let mut total_size = packet_container.clone().get_bytes().len();
			while total_size > 0 {
				let mut part_data: PacketData = PacketData::new();

				let cur_size = if total_size > 4089 { 4089 } else { total_size }; // Max buffer size == 4kb for the client

				part_data.write_uint8(0); // Data flag

				// workspace = ;
				part_data.write(&packet_container.read_bytes(cur_size));

				total_size -= cur_size; // Update the size

				final_data.write(&self.format_packet(
					0x600D,
					&part_data.get_bytes(),
					packet_container.encrypted,
				));

				parts += 1; // Track how many parts there are
			}

			// Write the final header packet to the front of the packet
			let mut final_header = PacketData::new();
			final_header.write_uint8(1); // Header flag
			final_header.write_uint16(parts as u16);
			final_header.write_uint16(packet_container.opcode);
			final_packet.write(&self.format_packet(
				0x600D,
				&final_header.get_bytes(),
				packet_container.encrypted,
			));

			// Finish the large packet of all the data
			final_packet.write(&final_data.get_bytes());

			// Return the collated data
			final_packet.get_bytes()
		} else {
			if !self.m_client_security && self.m_enc_opcodes.contains(&packet_container.opcode) {
				packet_container.encrypted = true;
			}

			self.format_packet(
				packet_container.opcode,
				&packet_container.clone().get_bytes(),
				packet_container.encrypted,
			)
		}
	}

	// Changes the 0x2001 identify packet data that will be sent out by
	// this security object.
	pub fn change_identity(&mut self, name: String, flag: u8) {
		self.m_identity_name = name;
		self.m_identity_flag = flag;
	}

	// Generates the security settings. This should only be called if the security object
	// is being used to process an incoming connection's data (server).
	pub fn generate_security(&mut self, blowfish: bool, security_bytes: bool, handshake: bool) {
		critical_section::with(|_cs| {
			let mut flags: SecurityFlags = SecurityFlags::new();
			if blowfish {
				flags.none = 0;
				flags.blowfish = 1;
			}
			if security_bytes {
				flags.none = 0;
				flags.security_bytes = 1;
			}
			if handshake {
				flags.none = 0;
				flags.handshake = 1;
			}
			if !blowfish && !security_bytes && !handshake {
				flags.none = 1;
			}
			self.generate_security_from_flags(flags);
		});
	}

	// Adds an encrypted opcode to the API. Any opcodes registered here will automatically
	// be encrypted as needed. Users should add the current Item Use opcode if they will be
	// mixing security modes.
	pub fn add_encrypted_opcode(&mut self, opcode: u16) {
		critical_section::with(|_cs| {
			if !self.m_enc_opcodes.contains(&opcode) {
				self.m_enc_opcodes.push(opcode);
			}
		});
	}

	// Queues a packet for processing to be sent. The data is simply formatted and processed during
	// the next call to get_packets_to_send.
	pub fn send(&mut self, packet: Packet) {
		if packet.opcode == 0x5000 || packet.opcode == 0x9000 {
			panic!("[SecurityAPI::send] handshake packets cannot be sent through this function.");
		}
		critical_section::with(|_cs| {
			self.m_outgoing_packets.push(packet);
		});
	}

	// Transfers raw incoming data into the security object. Call get_packets_to_recv to
	// obtain a list of ready to process packets.
	pub fn recv(&mut self, stream: &[u8]) {
		let mut m_pending_stream: PacketData = PacketData::new_data(stream.to_vec());
		let mut total_bytes: i32 = m_pending_stream.get_size() as i32;
		while total_bytes > 2 {
			let mut packet_encrypted: bool = false;

			let mut required_size: u16 = m_pending_stream.read_uint16();
			m_pending_stream.seek_set(0);

			if required_size & 0x8000 > 0 {
				required_size &= 0x7FFF;
				if self.m_security_flags.blowfish == 1 {
					required_size =
						(2 + Blowfish::get_output_length((required_size + 4) as usize)) as u16;
				} else {
					required_size += 6;
				}
				packet_encrypted = true;
			} else {
				required_size += 6;
			}

			// If we have enough bytes to extract the next packet
			if required_size <= total_bytes as u16 {
				if packet_encrypted && self.m_security_flags.blowfish == 1 {
					let mut index: u16 = 2;
					let mut left: u16 = required_size - 2;
					let mut work_buffer: &[u8];
					m_pending_stream.seek_set(2);
					while left > 0 {
						let slice = m_pending_stream.read_array(8);
						work_buffer = slice.as_ref().unwrap();
						m_pending_stream.seek_set(index as u64);
						m_pending_stream.write(&self.m_blowfish.decode(work_buffer, 0).unwrap());
						m_pending_stream.seek_set((index + 8) as u64);
						left -= 8;
						index += 8;
					}
					m_pending_stream.seek_set(0);
				}

				// Save the current packet's header
				let packet_size: u16 = m_pending_stream.read_uint16();
				let packet_opcode: u16 = m_pending_stream.read_uint16();
				let packet_security_count: u8 = m_pending_stream.read_uint8();
				let packet_security_crc: u8 = m_pending_stream.read_uint8();

				// Client object whose bytes the server might need to verify
				if self.m_client_security && self.m_security_flags.security_bytes == 1 {
					let mut whole_packet: Vec<u8> = m_pending_stream.clone().get_bytes()
						[0..(6 + (packet_size & 0x7FFF) as usize)]
						.to_vec();

					let expected_count: u8 = self.generate_count_byte(true);
					if packet_security_count != expected_count {
						panic!("[SilkroadSecurity::Recv] Count byte mismatch.");
					}

					if self.m_security_flags.security_bytes == 1
						&& self.m_security_flags.blowfish == 0
						&& self.m_enc_opcodes.contains(&packet_opcode)
					{
						whole_packet.splice(0..2, (packet_size | 0x8000).to_le_bytes());
						packet_encrypted = true;
					}

					whole_packet[5] = 0;
					let expected_crc: u8 = self.generate_check_byte(&whole_packet, 0, None);
					if packet_security_crc != expected_crc {
						panic!("[SilkroadSecurity::Recv] CRC byte mismatch.");
					}
				}

				// Save the current packet's data
				let mut packet_data: PacketData = PacketData::new_data(
					m_pending_stream.clone().get_bytes()[6..(6 + (packet_size & 0x7FFF)) as usize]
						.to_vec(),
				);

				// Sliding window update of remaining bytes
				let window: Vec<u8> = m_pending_stream.clone().get_bytes();
				m_pending_stream.reset(Some(window[(required_size as usize)..].to_vec()), None);
				m_pending_stream.seek_set(0);
				total_bytes -= required_size as i32;

				if packet_opcode == 0x5000 || packet_opcode == 0x9000
				// New logic processing!
				{
					self.handshake(packet_opcode, packet_data, packet_encrypted);
				} else {
					if self.m_client_security {
						// Make sure the client accepted the security system first
						if !self.m_accepted_handshake {
							panic!("[SilkroadSecurity::Recv] The client has not accepted the handshake.");
						}
					}
					if packet_opcode == 0x600D
					// Auto process massive messages for the user
					{
						let mode: u8 = packet_data.read_uint8();
						if mode == 1 {
							self.is_m_massive_header = true;
							self.m_massive_count = packet_data.read_uint16();
							self.m_massive_opcode = packet_data.read_uint16();
						} else {
							if !self.is_m_massive_header {
								panic!("[SilkroadSecurity::Recv] A malformed 0x600D packet was received.");
							}
							packet_data =
								PacketData::new_data(packet_data.get_bytes()[1..].to_vec());
							self.m_massive_count -= 1;
							if self.m_massive_count == 0 {
								let packet = Packet::new_full(
									self.m_massive_opcode,
									packet_data,
									Some(packet_encrypted),
									Some(true),
								);
								self.m_incoming_packets.push(packet);
								self.is_m_massive_header = false;
							}
						}
					} else {
						// Everything else
						self.m_incoming_packets.push(Packet::new_full(
							packet_opcode,
							packet_data,
							Some(packet_encrypted),
							Some(false),
						));
					}
				}
			} else {
				break; // Otherwise we are done in this loop
			}
		}
	}
}

#[cfg(test)]
mod tests {
	use crate::security::{Security, GLOBAL_SECURITY_TABLE};
	use crate::silkroad_security::security::{
		hi_byte_, hi_word_, lo_byte_, lo_word_, make_long_, make_long_long_, make_word_,
	};

	#[test]
	fn test_make_long_long_() {
		let a = 0x87654321;
		let b = 0x12345678;
		let expected: u64 = 0x1234567887654321;
		assert_eq!(expected, make_long_long_(a, b));
	}

	#[test]
	fn test_make_long_() {
		let a = 0x5678;
		let b = 0x1234;
		let expected: u32 = 0x12345678;
		assert_eq!(expected, make_long_(a, b));
	}

	#[test]
	fn test_make_word_() {
		let a = 0x34;
		let b = 0x12;
		let expected = 0x1234;
		assert_eq!(expected, make_word_(a, b));
	}

	#[test]
	fn test_lo_word_() {
		let initial = 0x12345678;
		let expected = 0x5678;
		assert_eq!(expected, lo_word_(initial));
	}

	#[test]
	fn test_hi_word_() {
		let initial = 0x12345678;
		let expected = 0x1234;
		assert_eq!(expected, hi_word_(initial));
	}

	#[test]
	fn test_lo_byte_() {
		let initial = 0x1234;
		let expected = 0x34;
		assert_eq!(expected, lo_byte_(initial));
	}

	#[test]
	fn test_hi_byte_() {
		let initial = 0x1234;
		let expected = 0x12;
		assert_eq!(expected, hi_byte_(initial));
	}

	#[test]
	fn test_key_transform_value() {
		let mut val: u64 = 4208818730835623940;
		let key: u32 = 12;
		let key_byte: u8 = 8;
		let expected_val = 8653798884025829404;
		Security::key_transform_value(&mut val, key, key_byte);
		assert_eq!(expected_val, val);
	}
}
