use byterepr::ByteRepr;
use num::Num;
use std::io::Error;
use std::string::FromUtf8Error;

pub trait BinaryReader {
	fn reset(&mut self, data: Option<Vec<u8>>, offset: Option<u32>);
	fn bytes_left(&mut self) -> u64;

	fn read<T: Num + ByteRepr>(&mut self) -> Result<T, Error>;
	fn read_array<T: Num + ByteRepr>(&mut self, length: usize) -> Result<Vec<T>, Error>;
	fn read_string(&mut self, length: usize) -> Result<String, FromUtf8Error>;
	fn read_int8(&mut self) -> i8;
	fn read_uint8(&mut self) -> u8;
	fn read_byte(&mut self) -> u8;
	fn read_int16(&mut self) -> i16;
	fn read_uint16(&mut self) -> u16;
	fn read_int32(&mut self) -> i32;
	fn read_uint32(&mut self) -> u32;
	fn read_int64(&mut self) -> i64;
	fn read_uint64(&mut self) -> u64;
	fn read_float(&mut self) -> f32;
	fn read_double(&mut self) -> f64;

	fn read_char(&mut self) -> char;
	fn read_bytes(&mut self, length: usize) -> Vec<u8>;
	fn read_ascii(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error>;
	fn read_utf8(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error>;
	fn read_utf16(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error>;
	fn read_unicode(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error>;
	fn read_utf32(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error>;
}
