use std::io::SeekFrom;

pub trait BinarySeeker {
	fn get_bytes(&mut self) -> Vec<u8>;
	fn get_position(&mut self) -> u64;
	fn get_size(&mut self) -> usize;
	fn check_offset_in_bounds(&mut self, offset: i64);
	fn seek(&mut self, seek_type: SeekFrom);
	fn seek_forward(&mut self, offset: i64);
	fn seek_backward(&mut self, offset: i64);
	fn seek_set(&mut self, offset: u64);
}
