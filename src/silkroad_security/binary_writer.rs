pub trait BinaryWriter {
	fn write(&mut self, val: &[u8]);
	fn write_string(&mut self, val: &str);
	fn write_int8(&mut self, val: i8);
	fn write_uint8(&mut self, val: u8);
	fn write_byte(&mut self, val: u8);
	fn write_int16(&mut self, val: i16);
	fn write_uint16(&mut self, val: u16);
	fn write_int32(&mut self, val: i32);
	fn write_uint32(&mut self, val: u32);
	fn write_int64(&mut self, val: i64);
	fn write_uint64(&mut self, val: u64);
	fn write_float(&mut self, val: f32);
	fn write_double(&mut self, val: f64);

	fn write_char(&mut self, val: char);
	fn write_bytes(&mut self, val: &[u8]);
	fn write_ascii(&mut self, val: &str);
	fn write_utf8(&mut self, val: &str);
	fn write_utf16(&mut self, val: &str);
	fn write_unicode(&mut self, val: &str);
	fn write_utf32(&mut self, val: &str);
}
