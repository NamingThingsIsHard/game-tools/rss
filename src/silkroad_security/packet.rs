use crate::silkroad_security::binary_reader::BinaryReader;
use crate::silkroad_security::binary_seeker::BinarySeeker;
use crate::silkroad_security::binary_writer::BinaryWriter;
use crate::silkroad_security::packet_data::PacketData;
use byterepr::ByteRepr;
use num::Num;
use std::fmt;
use std::fmt::Formatter;
use std::io::{Error, SeekFrom};
use std::string::FromUtf8Error;

#[derive(Debug, Clone)]
pub struct Packet {
	pub opcode: u16,
	pub encrypted: bool,
	pub massive: bool,
	pub data: PacketData,
}

impl Default for Packet {
	fn default() -> Self {
		Self::new(0)
	}
}

impl Packet {
	pub fn new(opcode: u16) -> Self {
		Self {
			opcode,
			encrypted: false,
			massive: false,
			data: PacketData::default(),
		}
	}

	pub fn new_flags(opcode: u16, encrypted: Option<bool>, massive: Option<bool>) -> Self {
		Self {
			opcode,
			encrypted: encrypted.unwrap_or_default(),
			massive: massive.unwrap_or_default(),
			data: PacketData::new(),
		}
	}

	pub fn new_full(
		opcode: u16,
		data: PacketData,
		encrypted: Option<bool>,
		massive: Option<bool>,
	) -> Self {
		Self {
			opcode,
			data,
			encrypted: encrypted.unwrap_or_default(),
			massive: massive.unwrap_or_default(),
		}
	}

	pub fn get_data(&mut self) -> PacketData {
		self.data.clone()
	}

	pub fn hex_dump(buffer: &[u8], offset: Option<usize>, count: Option<usize>) -> String {
		let offset = offset.unwrap_or(0);
		let count = count.unwrap_or(buffer.len());

		let bytes_per_line: usize = 16;
		let mut output: Vec<String> = vec!["".to_string()];
		let mut ascii_output: Vec<String> = vec!["".to_string()];
		let mut length: usize = count;
		if length % bytes_per_line != 0 {
			length += bytes_per_line - length % bytes_per_line;
		}
		for x in 0..length {
			if x % bytes_per_line == 0 {
				if x > 0 {
					output.push(format!("{}\n", ascii_output.join("")));
					ascii_output.clear();
				}
				if x != length {
					output.push(format!("{0:0<10}   ", x.to_string()).to_string());
				}
			}

			if x != length {
				output.push(format!("{0:0<10}   ", x.to_string()));
			}
			if x < count {
				output.push(format!("{:0<2X? }", buffer[offset + x]).to_string());

				let ch = char::from(buffer[offset + x]);
				if !ch.is_control() {
					ascii_output.push(format!("{}", ch).to_string());
				} else {
					ascii_output.push(".".to_string())
				}
			} else {
				output.push("   ".to_string());
				ascii_output.push(".".to_string());
			}
		}

		output.join("\n")
	}
}

impl fmt::Display for Packet {
	fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
		write!(
			f,
			"0x{0:2X},encrypted={},massive={}, {}", //,[{:?}]",
			self.opcode,
			self.encrypted,
			self.massive,
			// self.data.get_bytes()
			//     .iter()
			//     .map(|x| x.to_string())
			//     .collect()
		)
	}
}

impl BinarySeeker for Packet {
	fn get_bytes(&mut self) -> Vec<u8> {
		self.data.get_bytes()
	}

	fn get_position(&mut self) -> u64 {
		self.data.get_position()
	}

	fn get_size(&mut self) -> usize {
		self.data.get_size()
	}

	fn check_offset_in_bounds(&mut self, offset: i64) {
		self.data.check_offset_in_bounds(offset);
	}

	fn seek(&mut self, seek_type: SeekFrom) {
		self.data.seek(seek_type);
	}

	fn seek_forward(&mut self, offset: i64) {
		self.data.seek_forward(offset);
	}

	fn seek_backward(&mut self, offset: i64) {
		self.data.seek_backward(offset);
	}

	fn seek_set(&mut self, offset: u64) {
		self.data.seek_set(offset);
	}
}

impl BinaryReader for Packet {
	fn reset(&mut self, data: Option<Vec<u8>>, offset: Option<u32>) {
		self.data.reset(data, offset);
	}

	fn bytes_left(&mut self) -> u64 {
		self.data.bytes_left()
	}

	fn read<T: Num + ByteRepr>(&mut self) -> Result<T, Error> {
		self.data.read()
	}

	fn read_array<T: Num + ByteRepr>(&mut self, length: usize) -> Result<Vec<T>, Error> {
		self.data.read_array(length)
	}

	fn read_string(&mut self, length: usize) -> Result<String, FromUtf8Error> {
		self.data.read_string(length)
	}

	fn read_int8(&mut self) -> i8 {
		self.data.read_int8()
	}

	fn read_uint8(&mut self) -> u8 {
		self.data.read_uint8()
	}

	fn read_byte(&mut self) -> u8 {
		self.data.read_byte()
	}

	fn read_int16(&mut self) -> i16 {
		self.data.read_int16()
	}

	fn read_uint16(&mut self) -> u16 {
		self.data.read_uint16()
	}

	fn read_int32(&mut self) -> i32 {
		self.data.read_int32()
	}

	fn read_uint32(&mut self) -> u32 {
		self.data.read_uint32()
	}

	fn read_int64(&mut self) -> i64 {
		self.data.read_int64()
	}

	fn read_uint64(&mut self) -> u64 {
		self.data.read_uint64()
	}

	fn read_float(&mut self) -> f32 {
		self.data.read_float()
	}

	fn read_double(&mut self) -> f64 {
		self.data.read_double()
	}

	fn read_char(&mut self) -> char {
		self.data.read_char()
	}

	fn read_bytes(&mut self, length: usize) -> Vec<u8> {
		self.data.read_bytes(length)
	}

	fn read_ascii(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		self.data.read_ascii(length)
	}

	fn read_utf8(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		self.data.read_utf8(length)
	}

	fn read_utf16(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		self.data.read_utf16(length)
	}

	fn read_unicode(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		self.data.read_unicode(length)
	}

	fn read_utf32(&mut self, length: Option<u16>) -> Result<String, FromUtf8Error> {
		self.data.read_utf32(length)
	}
}

impl BinaryWriter for Packet {
	fn write(&mut self, val: &[u8]) {
		self.data.write(val);
	}

	fn write_string(&mut self, val: &str) {
		self.data.write_string(val);
	}

	fn write_int8(&mut self, val: i8) {
		self.data.write_int8(val);
	}

	fn write_uint8(&mut self, val: u8) {
		self.data.write_uint8(val);
	}

	fn write_byte(&mut self, val: u8) {
		self.data.write_byte(val);
	}

	fn write_int16(&mut self, val: i16) {
		self.data.write_int16(val);
	}

	fn write_uint16(&mut self, val: u16) {
		self.data.write_uint16(val);
	}

	fn write_int32(&mut self, val: i32) {
		self.data.write_int32(val);
	}

	fn write_uint32(&mut self, val: u32) {
		self.data.write_uint32(val);
	}

	fn write_int64(&mut self, val: i64) {
		self.data.write_int64(val);
	}

	fn write_uint64(&mut self, val: u64) {
		self.data.write_uint64(val);
	}

	fn write_float(&mut self, val: f32) {
		self.data.write_float(val);
	}

	fn write_double(&mut self, val: f64) {
		self.data.write_double(val);
	}

	fn write_char(&mut self, val: char) {
		self.data.write_char(val);
	}

	fn write_bytes(&mut self, val: &[u8]) {
		self.data.write_bytes(val);
	}

	fn write_ascii(&mut self, val: &str) {
		self.data.write_ascii(val);
	}

	fn write_utf8(&mut self, val: &str) {
		self.data.write_utf8(val);
	}

	fn write_utf16(&mut self, val: &str) {
		self.data.write_utf16(val);
	}

	fn write_unicode(&mut self, val: &str) {
		self.data.write_unicode(val);
	}

	fn write_utf32(&mut self, val: &str) {
		self.data.write_utf32(val);
	}
}

#[cfg(test)]
mod tests {
	use crate::silkroad_security::binary_reader::BinaryReader;
	use crate::silkroad_security::binary_seeker::BinarySeeker;
	use crate::silkroad_security::binary_writer::BinaryWriter;
	use crate::silkroad_security::packet_data::*;
	use byterepr::ByteRepr;
	use num::Num;
	use std::fmt::Debug;
	use std::io::SeekFrom;

	///SeekTests
	#[test]
	#[should_panic]
	fn test_bad_data() {
		let mut packet_data = PacketData::new();
		packet_data.reset(None, Some(1));
	}

	#[test]
	fn test_array_array() {
		let input_array: Vec<u8> = vec![1, 2, 3, 4, 5, 6];
		let mut reader = PacketData::new();
		reader.clone().reset(None, None);
		assert_eq!(reader.clone().get_position(), 0);
		assert_ne!(reader.get_bytes().to_vec(), input_array);
	}

	#[test]
	fn test_list() {
		let _list: Option<Vec<u8>> = Some(vec![1, 2, 3, 4, 5, 6]);
		let mut reader = PacketData::new();
		reader.reset(_list.clone(), None);

		assert_eq!(reader.bytes_left(), _list.unwrap().len() as u64);
		assert_eq!(reader.get_position(), 0);
	}

	#[test]
	fn test_bytes_new() {
		let _bytes: Option<Vec<u8>> = Some(vec![1, 2, 3, 4, 5, 6]);
		let mut reader = PacketData::new();
		reader.reset(_bytes.clone(), None);

		assert_eq!(reader.bytes_left(), _bytes.unwrap().len() as u64);
		assert_eq!(reader.get_position(), 0);
	}

	#[test]
	fn test_get_position() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5, 6]);
		let position = 2;
		reader.seek_set(position);

		assert_eq!(reader.get_position(), position);
	}

	#[test]
	fn test_get_size() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5, 6]);

		assert_eq!(reader.clone().get_bytes().len(), reader.get_size() as usize);
	}

	///    Test methods prefixed seek_
	#[test]
	fn test_seek_begin() {
		//Move to position 2
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_set(2);
		assert_eq!(reader.clone().get_position(), 2);
		reader.clone().seek_set(2);
		assert_eq!(reader.get_position(), 2);
	}

	#[test]
	fn test_seek_current() {
		//Move 2 forward
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_forward(2);
		assert_eq!(reader.clone().get_position(), 2);

		reader.seek_forward(2);
		assert_eq!(reader.get_position(), 4);
	}

	#[test]
	fn test_seek_end() {
		//Move 2 backwards from the end
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek(SeekFrom::End(-2));
		assert_eq!(reader.clone().get_position(), 3);

		reader.seek(SeekFrom::End(-1));
		assert_eq!(reader.get_position(), 4);
	}

	#[test]
	#[should_panic]
	fn test_seek_begin_bad_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_set(10);
		// reader.seek_set(-10);
	}

	#[test]
	#[should_panic]
	fn test_seek_current_bad_offset() {
		let mut reader = PacketData::new();
		reader.seek(SeekFrom::Current(-1));
	}

	#[test]
	#[should_panic]
	fn test_seek_end_bad_offset() {
		let mut reader = PacketData::new();
		reader.seek_backward(1);
	}

	#[test]
	#[should_panic]
	fn test_seek_bad_origin() {
		let mut reader = PacketData::new();
		reader.seek(SeekFrom::Current(1));
	}

	#[test]
	fn test_set_good_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		let expected_offset = 3;
		reader.seek_set(expected_offset);
		assert_eq!(reader.get_position(), expected_offset as u64);
	}

	#[test]
	#[should_panic]
	fn test_set_bad_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_set(10);
	}

	#[test]
	fn test_forward_good_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_forward(3);
		assert_eq!(reader.get_position(), 3);
	}

	#[test]
	#[should_panic]
	fn test_forward_bad_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_forward(10);
		reader.seek_forward(-10);
	}

	#[test]
	fn test_backward_good_offset() {
		let mut reader = PacketData::new_data(vec![1, 2, 3, 4, 5]);
		reader.seek_set(4);
		reader.seek_backward(3);
		assert_eq!(reader.get_position(), 1);
	}

	#[test]
	#[should_panic]
	fn test_backward_bad_offset() {
		let mut reader = PacketData::new();
		reader.seek_backward(10);
		reader.seek_backward(-10);
	}

	/// ReadTests

	fn test_read_func<T: Num + ByteRepr + Debug>(
		reader: &mut PacketData,
		expected_read_count: usize,
		expected_value: T,
	) {
		assert_read_is_good::<T>(reader, expected_read_count, expected_value);
		assert_read_is_out_of_bounds::<T>(reader);
	}

	#[should_panic]
	fn assert_read_is_out_of_bounds<T: Num + ByteRepr>(data: &mut PacketData) {
		let result = data.read::<T>();

		assert!(result.is_err())
	}

	fn assert_read_is_good<T: Num + ByteRepr + Debug>(
		reader: &mut PacketData,
		expected_read_count: usize,
		expected_value: T,
	) {
		let old_offset = reader.clone().get_position();
		let result: T = reader.read::<T>().unwrap();

		assert_eq!(expected_value, result);
		assert_eq!(
			reader.clone().get_position(),
			old_offset + expected_read_count as u64
		);
	}

	#[test]
	fn test_int8() {
		let mut reader = PacketData::new_data(vec![50]);
		test_read_func::<i8>(&mut reader, 1, 50);
	}

	#[test]
	fn test_uint8() {
		let mut reader = PacketData::new_data(vec![50]);
		test_read_func::<u8>(&mut reader, 1, 50);
	}

	#[test]
	fn test_byte() {
		let mut reader = PacketData::new_data(vec![128]);
		test_read_func::<u8>(&mut reader, 1, 128);
	}

	#[test]
	fn test_int16() {
		//Max positive value of signed 16bit int
		let mut reader = PacketData::new_data(vec![255, 127]);
		test_read_func::<i16>(&mut reader, 2, i16::MAX);
	}

	#[test]
	fn test_uint16() {
		//Max value of unsigned 16bit int
		let mut reader = PacketData::new_data(vec![255, 255]);
		test_read_func::<u16>(&mut reader, 2, u16::MAX);
	}

	#[test]
	fn test_int32() {
		//Max value of signed 32bit int
		let mut data = vec![255; 3];
		data.push(127);
		let mut reader = PacketData::new_data(data);
		test_read_func::<i32>(&mut reader, 4, i32::MAX);
	}

	#[test]
	fn test_uint32() {
		//Max value of unsigned 32bit int
		let mut reader = PacketData::new_data(vec![255; 4]);
		test_read_func::<u32>(&mut reader, 4, u32::MAX);
	}

	#[test]
	fn test_int64() {
		//Max value of signed 64bit int
		let mut data = vec![255; 7];
		data.push(127);
		let mut reader = PacketData::new_data(data);
		test_read_func::<i64>(&mut reader, 8, i64::MAX);
	}

	#[test]
	fn test_uint64() {
		//Max value of unsigned 64bit int
		let mut reader = PacketData::new_data(vec![255u8; 8]);
		test_read_func::<u64>(&mut reader, 8, u64::MAX);
	}

	#[test]
	fn test_float() {
		let value: f32 = 19.8391;
		let mut reader = PacketData::new_data(value.to_le_bytes().to_vec());
		let old_offset = reader.clone().get_position();

		let read_value = reader.read_float();
		assert_eq!(read_value, value); //, 4);
		assert_eq!(reader.clone().get_position(), old_offset + 4);

		assert_read_is_out_of_bounds::<f32>(&mut reader);
	}

	#[test]
	#[allow(clippy::excessive_precision)]
	fn test_double() {
		let value: f64 = 19.83914711113333344555555;
		let mut reader = PacketData::new_data(value.to_le_bytes().to_vec());
		let old_offset = reader.clone().get_position();

		let read_value = reader.read_double();
		assert_eq!(read_value, value); //, 23);
		assert_eq!(reader.clone().get_position(), old_offset + 8);

		assert_read_is_out_of_bounds::<f64>(&mut reader);
	}

	#[test]
	fn test_char() {
		let mut reader = PacketData::new_data(b"9".to_vec());
		test_read_func::<u8>(&mut reader, 1, b'9');
	}

	#[test]
	#[should_panic]
	fn test_write_uint8() {
		let mut p_d = PacketData::new();
		let val = 0xF;
		p_d.write_uint8(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_uint8());
		let _panic = p_d.read_uint8(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_int8() {
		let mut p_d = PacketData::new();
		let val = 0xF;
		p_d.write_int8(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_int8());
		let _panic = p_d.read_int8(); // should panic
	}

	#[test]
	#[should_panic]
	fn test_write_uint16() {
		let mut p_d = PacketData::new();
		let val = 0xFFFF;
		p_d.write_uint16(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_uint16());
		let _panic = p_d.read_uint16(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_int16() {
		let mut p_d = PacketData::new();
		let val = 0xFFF;
		p_d.write_int16(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_int16());
		let _panic = p_d.read_int16(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_uint32() {
		let mut p_d = PacketData::new();
		let val = 0xFFFFFFFF;
		p_d.write_uint32(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_uint32());
		let _panic = p_d.read_uint32(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_int32() {
		let mut p_d = PacketData::new();
		let val = 0xFFFFFFF;
		p_d.write_int32(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_int32());
		let _panic = p_d.read_int32(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_uint64() {
		let mut p_d = PacketData::new();
		let val = 0xFFFFFFFFFFFFFFFF;
		p_d.write_uint64(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_uint64());
		let _panic = p_d.read_uint64(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_int64() {
		let mut p_d = PacketData::new();
		let val = 0xFFFFFFFFFFFFFFF;
		p_d.write_int64(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_int64());
		let _panic = p_d.read_int64(); // should panic;
	}

	#[test]
	#[should_panic]
	fn test_write_float() {
		let mut p_d = PacketData::new();
		let val = 128.128;
		p_d.write_float(val);
		p_d.seek_set(0);
		let val = 128.128;
		p_d.write_float(val);
		let _panic = p_d.read_float(); // should panic
	}

	#[test]
	#[should_panic]
	fn test_write_double() {
		let mut p_d = PacketData::new();
		let val = 256.256;
		p_d.write_double(val);
		p_d.seek_set(0);
		let val = 256.256;
		p_d.write_double(val);
		let _panic = p_d.read_double(); // should panic
	}

	#[test]
	#[should_panic]
	fn test_write_ascii() {
		let mut p_d = PacketData::new();
		let val = "test";
		p_d.write_ascii(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_ascii(None).unwrap());
		let _panic = p_d.read_ascii(Some(1)); // should panic
	}

	#[test]
	#[should_panic]
	fn test_write_unicode() {
		let mut p_d = PacketData::new();
		let val = "test";
		p_d.write_unicode(val);
		p_d.seek_set(0);
		assert_eq!(val, p_d.read_unicode(None).unwrap());
		let _panic = p_d.read_unicode(Some(1)); // should panic
	}
}
