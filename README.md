pySilkroadSecurity
==================

Rust SilkroadSecurity ports Drew's SilkroadSecurity API to Rust.

##Requirements
------------

1. rust programming language.

### Linux, Mac OSX
--------

- Recommended: Install `rustup` using this line (see
  the [rust startup guide](https://www.rust-lang.org/learn/get-started)):

```curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh```

- or Install `rust` using a package manager.

### Windows
--------

- Follow the guide for [other Rust installation methods](https://forge.rust-lang.org/infra/other-installation-methods.html)

### RaspberryPi (Raspbian)
----------------------

1. Follow the Linux guide.
2. Use a single thread to compile

## Examples
========

**pySilkroadStats.py**

This small project shows you how the SilkroadSecurity API is to be used from Python. It will connect to iSRO and display the server list. This can be easily added to by adding a few lines to log into the servers and join the game world.

**pySilkroadProxy.py**

This project accepts connections on TCP port 15779 and will create a proxy between the Silkroad client and the Silkroad game servers. This will allow you to view all packets going to and from Silkroad. This project can also be easily modified to filter packets for a private server; although, I would recommend rewriting the network code to not use select() if you end up needing to handle more than 100 simultaneous connections.

Usage in Your Own Project
-------------------------

Copy pySilkroadSecurity.so and stream.py to your own project folder and import them like so:

```
from pySilkroadSecurity import SilkroadSecurity
from stream import *
```

Warnings
========

* iSRO/SilkroadR client will crash after loading the game world if HackShield is disabled using edxSilkroadLoader5 (it's missing one client patch that was added recently)
* Stream classes have not been extensively tested/used and may have bugs
